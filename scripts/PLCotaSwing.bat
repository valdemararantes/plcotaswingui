@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  PLCotaSwing startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

@rem Add default JVM options here. You can also use JAVA_OPTS and PL_COTA_SWING_UI_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS="-Dfile.encoding=utf-8" "-Xms256M" "-Xmx1024M" -Duser.language=pt_br

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windowz variants

if not "%OS%" == "Windows_NT" goto win9xME_args
if "%@eval[2+2]" == "4" goto 4NT_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*
goto execute

:4NT_args
@rem Get arguments from the 4NT Shell from JP Software
set CMD_LINE_ARGS=%$

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\PLCotaSwingUI-1.0.0.jar;%APP_HOME%\lib\logback-classic-1.1.6.jar;%APP_HOME%\lib\PLCotaAPI-1.0.0.jar;%APP_HOME%\lib\commons-io-2.4.jar;%APP_HOME%\lib\weld-se-2.3.2.Final.jar;%APP_HOME%\lib\swingx-all-1.6.5-1.jar;%APP_HOME%\lib\jopt-simple-5.0.jar;%APP_HOME%\lib\logback-core-1.1.6.jar;%APP_HOME%\lib\slf4j-api-1.7.18.jar;%APP_HOME%\lib\GalgoCXFUtils-3.0.0.jar;%APP_HOME%\lib\guava-18.0.jar;%APP_HOME%\lib\jackson-mapper-asl-1.9.13.jar;%APP_HOME%\lib\UtilMvn-2.1.0.jar;%APP_HOME%\lib\GalgoWSGenerated-5.0.0.jar;%APP_HOME%\lib\ObjectUtils-2.0.jar;%APP_HOME%\lib\jodd-bean-3.6.6.jar;%APP_HOME%\lib\testng-6.9.4.jar;%APP_HOME%\lib\cxf-rt-frontend-jaxws-2.7.8.jar;%APP_HOME%\lib\cxf-rt-management-2.7.8.jar;%APP_HOME%\lib\cxf-rt-ws-policy-2.7.8.jar;%APP_HOME%\lib\cxf-rt-transports-http-hc-2.7.3.jar;%APP_HOME%\lib\cxf-rt-ws-security-2.7.8.jar;%APP_HOME%\lib\commons-beanutils-1.9.2.jar;%APP_HOME%\lib\commons-lang-2.6.jar;%APP_HOME%\lib\commons-collections-3.2.1.jar;%APP_HOME%\lib\jackson-core-asl-1.9.13.jar;%APP_HOME%\lib\joda-time-2.8.jar;%APP_HOME%\lib\gson-2.3.1.jar;%APP_HOME%\lib\bsh-2.0b4.jar;%APP_HOME%\lib\jcommander-1.48.jar;%APP_HOME%\lib\xml-resolver-1.2.jar;%APP_HOME%\lib\asm-3.3.1.jar;%APP_HOME%\lib\cxf-api-2.7.8.jar;%APP_HOME%\lib\cxf-rt-core-2.7.8.jar;%APP_HOME%\lib\cxf-rt-bindings-soap-2.7.8.jar;%APP_HOME%\lib\cxf-rt-bindings-xml-2.7.8.jar;%APP_HOME%\lib\cxf-rt-frontend-simple-2.7.8.jar;%APP_HOME%\lib\cxf-rt-ws-addr-2.7.8.jar;%APP_HOME%\lib\neethi-3.0.2.jar;%APP_HOME%\lib\cxf-rt-transports-http-2.7.3.jar;%APP_HOME%\lib\httpcore-nio-4.2.2.jar;%APP_HOME%\lib\httpasyncclient-4.0-beta3.jar;%APP_HOME%\lib\wss4j-1.6.13.jar;%APP_HOME%\lib\commons-logging-1.1.1.jar;%APP_HOME%\lib\cxf-rt-databinding-jaxb-2.7.8.jar;%APP_HOME%\lib\httpcore-4.2.2.jar;%APP_HOME%\lib\httpclient-4.2.1.jar;%APP_HOME%\lib\xmlsec-1.5.6.jar;%APP_HOME%\lib\jaxb-impl-2.1.13.jar;%APP_HOME%\lib\commons-codec-1.6.jar;%APP_HOME%\lib\jodd-core-3.6.6.jar;%APP_HOME%\lib\woodstox-core-asl-4.2.0.jar;%APP_HOME%\lib\xmlschema-core-2.0.3.jar;%APP_HOME%\lib\geronimo-javamail_1.4_spec-1.7.1.jar;%APP_HOME%\lib\wsdl4j-1.6.3.jar;%APP_HOME%\lib\stax2-api-3.1.1.jar

@rem Execute PLCotaSwing
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %PL_COTA_SWING_UI_OPTS%  -classpath "%CLASSPATH%" br.com.galgo.plcotaswingui.ui.PLCota %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable PL_COTA_SWING_UI_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%PL_COTA_SWING_UI_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
