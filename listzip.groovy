import java.util.zip.ZipFile

ZipFile file = new ZipFile("build/distributions/PLCotaUI-2.0.0_20160504.zip")

file.entries().findAll {!it.directory && it.name.contains('/bin/')}.each {
	println "$it.name"
}