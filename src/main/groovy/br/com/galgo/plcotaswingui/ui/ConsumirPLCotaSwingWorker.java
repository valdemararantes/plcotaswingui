package br.com.galgo.plcotaswingui.ui;

import br.com.galgo.plcota.WSConsumirPLCota;
import com.galgo.cxfutils.ws.AmbienteEnum;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.swing.*;
import java.util.Date;
import java.util.List;

/**
 * Created by valdemar.arantes on 30/11/2015.
 */
public class ConsumirPLCotaSwingWorker extends SwingWorker<String, Void> {

    private static final Logger log = LoggerFactory.getLogger(ConsumirPLCotaSwingWorker.class);
    private List<Integer> cdSTIList;
    private Date dtInit;
    private Date dtFin;
    private Date dtInitEnvio;
    private Date dtFinalEnvio;
    private int qtMax;
    private Integer firstPage = 1;
    private Integer lastPage;
    private Integer statusInfo = 0;
    private @Inject transient WSConsumirPLCota wsConsPLCota;

    /**
     * Computes a result, or throws an exception if unable to do so.
     * <p>
     * <p>
     * Note that this method is executed only once.
     * <p>
     * <p>
     * Note: this method is executed in a background thread.
     *
     * @return the computed result
     * @throws Exception if unable to compute a result
     */
    @Override
    protected String doInBackground() throws Exception {
        return null;
    }

    public ConsumirPLCotaSwingWorker() {}

    public ConsumirPLCotaSwingWorker initialize(List<Integer> cdSTIList, Date dtInit, Date dtFin, Date dtInitEnvio, Date dtFinalEnvio,
        int qtMax, Integer firstPage, Integer lastPage, int statusInfo, AmbienteEnum ambiente) {
        this.cdSTIList = cdSTIList;
        this.dtInit = dtInit;
        this.dtFin = dtFin;
        this.dtInitEnvio = dtInitEnvio;
        this.dtFinalEnvio = dtFinalEnvio;
        this.qtMax = qtMax;
        this.firstPage = firstPage;
        this.lastPage = NumberUtils.INTEGER_ZERO.equals(lastPage) ? null : lastPage;
        if (this.lastPage == null) {
            log.info("Todas as páginas serão consumidas");
        }
        this.statusInfo = statusInfo;
        wsConsPLCota.setAmbiente(ambiente);
//        wsConsPLCota.setAmbiente(grpAmbiente.isSelected(radioHomolog.getModel()) ? AmbienteEnum.HOMOLOGACAO :
//                                 AmbienteEnum.PRODUCAO);
        return this;
    }
}
