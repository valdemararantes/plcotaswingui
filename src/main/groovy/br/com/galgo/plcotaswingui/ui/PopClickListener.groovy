package br.com.galgo.plcotaswingui.ui

import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

/**
 * Created by valdemar.arantes on 25/05/2016.*/
class PopClickListener extends MouseAdapter {
    public void mousePressed(MouseEvent e){
        //if (e.isPopupTrigger())
        //    doPop(e);
    }

    public void mouseReleased(MouseEvent e){
        //if (e.isPopupTrigger())
            doPop(e);
    }

    private void doPop(MouseEvent e){
        PreferencesPopupMenu menu = new PreferencesPopupMenu();
        menu.show(e.getComponent(), e.getX(), e.getY());
    }
}
