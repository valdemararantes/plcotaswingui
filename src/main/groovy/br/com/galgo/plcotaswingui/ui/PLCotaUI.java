package br.com.galgo.plcotaswingui.ui;

import br.com.galgo.plcota.WSConsumirPLCota;
import br.com.galgo.plcota.WSListener;
import br.com.galgo.plcota.utils.Atuacao;
import br.com.galgo.plcota.utils.FundoUtils;
import br.com.galgo.plcota.utils.PLCotaUtils;
import br.com.galgo.plcota.utils.TimeInterval;
import br.com.galgo.plcotaswingui.utils.AppConfig;
import br.com.galgo.plcotaswingui.utils.Config;
import br.com.galgo.plcotaswingui.utils.ConsumidorPLCota;
import br.com.galgo.plcotaswingui.utils.DataHoraEnvioInitializer;
import br.com.galgo.plcotaswingui.utils.ExternalConfigFile;
import br.com.galgo.plcotaswingui.utils.FileChooserUtils;
import br.com.galgo.plcotaswingui.utils.FirstExecution;
import br.com.galgo.plcotaswingui.utils.VpnHandler;
import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType;
import br.com.stianbid.serviceplcota.ConsumirFaultMsg;
import com.galgo.cxfutils.ws.AmbienteEnum;
import com.galgo.utils.ApplicationException;
import com.galgo.utils.cert_digital.TrustAllCerts;
import com.galgo.utils.xml.SOAPUtils;
import com.google.common.collect.Lists;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import groovy.util.MapEntry;
import iso.std.iso._20022.tech.xsd.reda_001_001.Document;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.event.Observes;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.swing.*;
import javax.xml.bind.JAXB;
import javax.xml.ws.soap.SOAPFaultException;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.apache.commons.lang.math.NumberUtils.INTEGER_ZERO;

/**
 * @author valdemar.arantes
 */
@Named
@Singleton
@SuppressWarnings("non-transient")
public class PLCotaUI extends JFrame implements WSListener {

    private static final Logger log = LoggerFactory.getLogger(PLCotaUI.class);
    private static final String MSG_QTD_FUNDO_RETOR = "Quantidade de registros retornados";
    private static final String MSG_NENHUM_FUNDO_RETOR = "Nenhum registro retornado para este período";
    private static final String ERRO_STR = "Erro: ";
    private final static MouseAdapter doNothingMouseAdapter = new MouseAdapter() {
    };
    private final static KeyAdapter doNothingKeyAdapter = new KeyAdapter() {
    };
    private static PLCotaUI instance;
    private static LocalDateTime appStartingTime;

    private AppConfig appConfig;
    private Date dtTmInitEnvio;
    private Date dtTmFinalEnvio;
    private File savedFile;
    private int sliceTime;
    private boolean isTeste = "true".equalsIgnoreCase(System.getProperty("plcota.teste"));
    private File folderToSave;
    private Stage stage;
    public static com.galgo.utils.Config externalConfig;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.jdesktop.swingx.JXButton btnConsumir;
    private javax.swing.JButton btnSelecionarPasta;
    private javax.swing.JComboBox<String> comboAtuacaoComprom;
    private javax.swing.JComboBox comboStatus;
    private org.jdesktop.swingx.JXDatePicker dtFinal;
    private org.jdesktop.swingx.JXDatePicker dtInitial;
    private javax.swing.ButtonGroup grpAmbiente;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanelFooter;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelInput;
    private javax.swing.JPanel jPanelInputIn;
    private javax.swing.JPanel jPanelOutput;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private org.jdesktop.swingx.JXDatePicker jxDtFinalEnvio;
    private org.jdesktop.swingx.JXDatePicker jxDtInitEnvio;
    private javax.swing.JLabel lblAmbiente;
    private javax.swing.JLabel lblAtuacaoComprom;
    private javax.swing.JLabel lblDataBaseInfo;
    private javax.swing.JLabel lblDtFinal;
    private javax.swing.JLabel lblDtFinalEnvio;
    private javax.swing.JLabel lblDtHrEnvio;
    private javax.swing.JLabel lblDtInitEnvio;
    private javax.swing.JLabel lblDtInitial;
    private javax.swing.JLabel lblFormatoArquivo;
    private javax.swing.JLabel lblListCodSTI;
    private javax.swing.JLabel lblPasta;
    private javax.swing.JLabel lblPreferences;
    private javax.swing.JLabel lblStatusInfo;
    private javax.swing.JLabel lblTmFinalEnvio;
    private javax.swing.JLabel lblTmInitEnvio;
    private javax.swing.JLabel lblVersion;
    private javax.swing.JMenuItem menuItemClearTextArea;
    private javax.swing.JScrollPane paneListCodSTI;
    private javax.swing.JPasswordField pwdPassword;
    private javax.swing.JRadioButton radioHomolog;
    private javax.swing.JRadioButton radioProducao;
    private javax.swing.JSpinner spinTmFinalEnvio;
    private javax.swing.JSpinner spinTmInitEnvio;
    private javax.swing.JPopupMenu textAreaPopupMenu;
    private org.jdesktop.swingx.JXTextArea textAreaPrompt;
    private javax.swing.JTextArea txtCdSTIs;
    private javax.swing.JTextField txtPasta;
    private org.jdesktop.swingx.JXLabel xlblLogin;
    private org.jdesktop.swingx.JXLabel xlblPassword;
    private org.jdesktop.swingx.JXTextField xtxtLogin;
    // End of variables declaration//GEN-END:variables

    /**
     * Creates new form PLCotaUI
     */
    public PLCotaUI() {
        super();
        instance = this;
        log.info(AppConfig.getInstance().getProperty("app.version"));

        try {
            UIManager.setLookAndFeel(new WindowsLookAndFeel());
            UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Tahoma", Font.PLAIN, 14));
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        // Método criado pelo Netbeans. Não editar manualmente.
        initComponents();

        try {
            this.setTitle(this.getTitle());

            // Inicializações específicas da primeira execução
            FirstExecution.execute();

            // Aceitando todos os certificados SSL
            TrustAllCerts.doIt();

            // Centralizando a janela quando é aberta
            setLocationRelativeTo(null);

            // Intervalo de fatiamento do consumo definido em app-config.properties
            sliceTime = Config.getInstance().getInteger("slice.time", 0);
            log.debug("sliceTime = {}", sliceTime);

            //RequiredFields.markLabels(lblAmbiente, xlblLogin, xlblPassword, lblStatusInfo, lblAtuacaoComprom);

            // Para evitar a validação do que foi digitado pelo usuário
            txtPasta.setEditable(false);
            txtPasta.setToolTipText("Selecione a pasta utilizando o botão ao lado (...)");

            // Preenchimento dos campos definido nos requisitos do app
            initFieldsValues();

            lblVersion.setText(AppConfig.getInstance().getProperty("app.version"));

            handleDateAndTimeComponentsVisibility();

            lblPreferences.addMouseListener(new PopClickListener());

            // Ver https://rterp.wordpress.com/2015/04/04/javafx-toolkit-not-initialized-solved/ :
            // JavaFX Toolkit Not Initialized
            final JFXPanel jfxPanel = new JFXPanel();

            // Necessário para que a janela de preferência possa ser apresentada mais de uma vez.
            Platform.setImplicitExit(false);

            // Ver https://community.oracle.com/thread/2375429?tstart=0
            // Platform.runLater precisa que a instância aberta esteja sempre viva
            /*Platform.runLater(() -> {
                stage = new Stage(StageStyle.TRANSPARENT);
                stage.setWidth(10);
                stage.setHeight(10);
                stage.setIconified(false);
                stage.show();
            });*/

        } catch (Exception e) {
            appendPromptMsg(ERRO_STR + ExceptionUtils.getRootCauseMessage(e));
            log.error(null, e);
        }
    }

    public static PLCotaUI getInstance() {
        return instance;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws IOException {
        appStartingTime = LocalDateTime.now();
        log.info("#################################### Iniciando PLCotaUI ###################");
        int returnNumber = 0;
        log.info("Iniciando o aplicativo em {}", appStartingTime);
        try {
            ExternalConfigFile.loadExternalConfigFile();
            final Properties defaultProps = new Properties();
            defaultProps.load(PLCotaUI.class.getResourceAsStream("/config_default.properties"));
            Config.newInstance("plcota", defaultProps);
            EventQueue.invokeLater(() -> new PLCotaUI().setVisible(true));
        } catch (Throwable e) {
            log.error(e.toString(), e);
            returnNumber = -1;
        } finally {
            log.info("Aplicativo encerrado em {}", LocalDateTime.now());
        }
    }

    public void fileSavedEventObserver(@Observes File savedFile) {
        log.trace("Evento de arquivo salvo. savedFile={}", savedFile.getAbsolutePath());
        this.savedFile = savedFile;
    }

    @Override
    public void notify(Event evt, String notifyMsg) {
        appendPromptMsg(notifyMsg + "\r\n");
    }

    //public void saySomething(@Observes ContainerInitialized event) {
    public void saySomething() {
        log.info("#################################### Iniciando PLCotaUI ###################");
        EventQueue.invokeLater(() -> PLCotaUI.this.setVisible(true));
    }

    /**
     * Retorna um objeto Date com a data obtida do parâmetro dt e a hora do parâmetro tm
     *
     * @param dt Data inicial
     * @param tm Hora a ser adicionada à data inicial
     * @return Data inicial + Hora
     */
    private Date addTimeToDate(Date dt, Date tm) {
        if (dt == null) {
            return null;
        }
        Calendar calDt = Calendar.getInstance();
        calDt.setTime(dt);

        Calendar calTm = Calendar.getInstance();
        calTm.setTime(tm);


        calDt.set(Calendar.HOUR, calTm.get(Calendar.HOUR_OF_DAY));
        calDt.set(Calendar.MINUTE, calTm.get(Calendar.MINUTE));
        calDt.set(Calendar.SECOND, calTm.get(Calendar.SECOND));
        calDt.set(Calendar.MILLISECOND, calTm.get(Calendar.MILLISECOND));

        return calDt.getTime();
    }

    private void addTimesToDates() {
        if (jxDtInitEnvio.getDate() != null) {
            dtTmInitEnvio = addTimeToDate(jxDtInitEnvio.getDate(), (Date) spinTmInitEnvio.getValue());
        } else {
            dtTmInitEnvio = null;
        }

        if (jxDtFinalEnvio.getDate() != null) {
            dtTmFinalEnvio = addTimeToDate(jxDtFinalEnvio.getDate(), (Date) spinTmFinalEnvio.getValue());
        } else {
            dtTmFinalEnvio = null;
        }
    }

    private void appendPromptMsg(String msg) {
        textAreaPrompt.append(msg);
        textAreaPrompt.setCaretPosition(textAreaPrompt.getText().length());
    }

    @SuppressWarnings("unchecked")
    private void chooseAmbiente(java.awt.event.ActionEvent evt) {
        Config conf = Config.getInstance();
        Map.Entry<String, String> entry;
        if (radioProducao.isSelected()) {
            entry = new MapEntry(Config.AMBIENTE_KEY, Config.AMBIENTE_VALUE_PROD);
        } else {
            entry = new MapEntry(Config.AMBIENTE_KEY, Config.AMBIENTE_VALUE_HOMOL);
        }
        log.info("Salvando nas configurações do usuário: {} = {} ...", entry.getKey(), entry.getValue());
        conf.addProperty(entry.getKey(), entry.getValue());
    }

    private void formWindowOpened(java.awt.event.WindowEvent evt) {                                  
        Config conf = Config.getInstance();
        try {
            xtxtLogin.setText(conf.getUsername());
        } catch (ApplicationException e) {
            log.warn(e.getMessage());
        } catch (Exception e) {
            log.error(null, e);
        }


        try {
            pwdPassword.setText(conf.getPassword());
        } catch (ApplicationException e) {
            log.warn(e.getMessage());
        } catch (Exception e) {
            log.error(null, e);
        }
    }                                 

    /**
     * Esonde os caracteres dos componentes spinners com os tempos quando os componentes de datas são
     * deixados em branco.
     */
    private void handleDateAndTimeComponentsVisibility() {
        jxDtInitEnvio.getEditor().addPropertyChangeListener(evt -> {
            setSpinEditorTextVisible(lblTmInitEnvio, spinTmInitEnvio, evt.getNewValue() != null);
        });
        jxDtFinalEnvio.getEditor().addPropertyChangeListener(evt -> {
            setSpinEditorTextVisible(lblTmFinalEnvio, spinTmFinalEnvio, evt.getNewValue() != null);
        });
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpAmbiente = new javax.swing.ButtonGroup();
        textAreaPopupMenu = new javax.swing.JPopupMenu();
        menuItemClearTextArea = new javax.swing.JMenuItem();
        jPanelHeader = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblPreferences = new javax.swing.JLabel();
        jPanelInput = new javax.swing.JPanel();
        lblAmbiente = new javax.swing.JLabel();
        radioHomolog = new javax.swing.JRadioButton();
        radioProducao = new javax.swing.JRadioButton();
        jPanelInputIn = new javax.swing.JPanel();
        lblDataBaseInfo = new javax.swing.JLabel();
        lblDtInitial = new javax.swing.JLabel();
        dtInitial = new org.jdesktop.swingx.JXDatePicker();
        lblDtFinal = new javax.swing.JLabel();
        dtFinal = new org.jdesktop.swingx.JXDatePicker();
        xlblLogin = new org.jdesktop.swingx.JXLabel();
        xtxtLogin = new org.jdesktop.swingx.JXTextField();
        xlblPassword = new org.jdesktop.swingx.JXLabel();
        pwdPassword = new javax.swing.JPasswordField();
        btnConsumir = new org.jdesktop.swingx.JXButton();
        lblListCodSTI = new javax.swing.JLabel();
        paneListCodSTI = new javax.swing.JScrollPane();
        txtCdSTIs = new javax.swing.JTextArea();
        lblDtInitEnvio = new javax.swing.JLabel();
        jxDtInitEnvio = new org.jdesktop.swingx.JXDatePicker();
        lblDtHrEnvio = new javax.swing.JLabel();
        lblTmInitEnvio = new javax.swing.JLabel();
        spinTmInitEnvio = new javax.swing.JSpinner();
        lblDtFinalEnvio = new javax.swing.JLabel();
        jxDtFinalEnvio = new org.jdesktop.swingx.JXDatePicker();
        lblTmFinalEnvio = new javax.swing.JLabel();
        spinTmFinalEnvio = new javax.swing.JSpinner();
        lblStatusInfo = new javax.swing.JLabel();
        comboStatus = new javax.swing.JComboBox();
        lblAtuacaoComprom = new javax.swing.JLabel();
        comboAtuacaoComprom = new javax.swing.JComboBox<>();
        lblPasta = new javax.swing.JLabel();
        txtPasta = new javax.swing.JTextField();
        btnSelecionarPasta = new javax.swing.JButton();
        lblFormatoArquivo = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jPanelOutput = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        textAreaPrompt = new org.jdesktop.swingx.JXTextArea();
        jPanelFooter = new javax.swing.JPanel();
        lblVersion = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();

        menuItemClearTextArea.setText("Limpar");
        menuItemClearTextArea.setToolTipText("");
        menuItemClearTextArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemClearTextAreaActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(menuItemClearTextArea);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Consumir PL Cota");
        setMinimumSize(new java.awt.Dimension(588, 624));
        setName("frMain"); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanelHeader.setBackground(new java.awt.Color(20, 135, 216));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/galgo_banner.png"))); // NOI18N
        jLabel1.setText("jLabel1");

        lblPreferences.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPreferences.setForeground(new java.awt.Color(255, 255, 255));
        lblPreferences.setText("...");
        lblPreferences.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPreferencesMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelHeaderLayout = new javax.swing.GroupLayout(jPanelHeader);
        jPanelHeader.setLayout(jPanelHeaderLayout);
        jPanelHeaderLayout.setHorizontalGroup(
            jPanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelHeaderLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 475, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblPreferences)
                .addContainerGap())
        );
        jPanelHeaderLayout.setVerticalGroup(
            jPanelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelHeaderLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblPreferences)
                .addContainerGap())
        );

        lblAmbiente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblAmbiente.setText("Ambiente:");

        grpAmbiente.add(radioHomolog);
        radioHomolog.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        radioHomolog.setSelected(true);
        radioHomolog.setText("Homologação");
        radioHomolog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioHomologActionPerformed(evt);
            }
        });

        grpAmbiente.add(radioProducao);
        radioProducao.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        radioProducao.setText("Produção");
        radioProducao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioProducaoActionPerformed(evt);
            }
        });

        jPanelInputIn.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelInputIn.setToolTipText("");

        lblDataBaseInfo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        lblDataBaseInfo.setLabelFor(dtInitial);
        lblDataBaseInfo.setText("Data Base da Informação");

        lblDtInitial.setText("Inicial:");

        dtInitial.setToolTipText("d(d)/m(m)/aaaa");
        dtInitial.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        lblDtFinal.setText("Final:");

        dtFinal.setToolTipText("d(d)/m(m)/aaaa");
        dtFinal.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        xlblLogin.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xlblLogin.setText("Usuário (*):");
        xlblLogin.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        xtxtLogin.setToolTipText("");
        xtxtLogin.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        xlblPassword.setText("Senha (*):");
        xlblPassword.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnConsumir.setText("Consumir");
        btnConsumir.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnConsumir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsumirActionPerformed(evt);
            }
        });

        lblListCodSTI.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblListCodSTI.setText("Códigos STI dos Fundos de Investimento:");

        txtCdSTIs.setColumns(20);
        txtCdSTIs.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtCdSTIs.setLineWrap(true);
        txtCdSTIs.setRows(5);
        paneListCodSTI.setViewportView(txtCdSTIs);

        lblDtInitEnvio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblDtInitEnvio.setText("Data inicial:");

        jxDtInitEnvio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        lblDtHrEnvio.setFont(lblDataBaseInfo.getFont());
        lblDtHrEnvio.setLabelFor(jxDtInitEnvio);
        lblDtHrEnvio.setText("Data/Hora de Envio");

        lblTmInitEnvio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblTmInitEnvio.setText("Hora inicial:");

        spinTmInitEnvio.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        spinTmInitEnvio.setModel(new javax.swing.SpinnerDateModel(new java.util.Date(1459800957442L), null, null, java.util.Calendar.HOUR_OF_DAY));
        spinTmInitEnvio.setToolTipText("Formato HH:mm:ss");
        spinTmInitEnvio.setEditor(new javax.swing.JSpinner.DateEditor(spinTmInitEnvio, "HH:mm:ss"));
        spinTmInitEnvio.setVerifyInputWhenFocusTarget(false);
        //JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(spinTmInitEnvio, "HH:mm:ss.SSS");
        //spinTmInitEnvio.setEditor(timeEditor);

        lblDtFinalEnvio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblDtFinalEnvio.setText("Data final:");

        jxDtFinalEnvio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        lblTmFinalEnvio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblTmFinalEnvio.setText("Hora final:");

        spinTmFinalEnvio.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        spinTmFinalEnvio.setModel(new javax.swing.SpinnerDateModel(new java.util.Date(), null, null, java.util.Calendar.HOUR_OF_DAY));
        spinTmFinalEnvio.setToolTipText("Formato HH:mm:ss");
        spinTmFinalEnvio.setEditor(new javax.swing.JSpinner.DateEditor(spinTmFinalEnvio, "HH:mm:ss"));

        lblStatusInfo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblStatusInfo.setText("Status da Informação (*):");

        comboStatus.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        comboStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Todas", "Enviada", "Reenviada", "Cancelada Man.", "Cancelada Aut.", "Rejeitada" }));

        lblAtuacaoComprom.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblAtuacaoComprom.setText("Atuação no Compromisso (*):");

        comboAtuacaoComprom.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        comboAtuacaoComprom.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Consumidor", "Provedor" }));

        lblPasta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblPasta.setText("Salvar na pasta (*):");

        txtPasta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtPasta.setToolTipText("Clique aqui para digitar a pasta");

        btnSelecionarPasta.setText("...");
        btnSelecionarPasta.setToolTipText("Clique aqui para selecionar a pasta");
        btnSelecionarPasta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarPastaActionPerformed(evt);
            }
        });

        lblFormatoArquivo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblFormatoArquivo.setText("Formato do Arquivo:");

        jComboBox1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "XML", "CSV" }));

        javax.swing.GroupLayout jPanelInputInLayout = new javax.swing.GroupLayout(jPanelInputIn);
        jPanelInputIn.setLayout(jPanelInputInLayout);
        jPanelInputInLayout.setHorizontalGroup(
            jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInputInLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jSeparator2)
                    .addComponent(paneListCodSTI)
                    .addGroup(jPanelInputInLayout.createSequentialGroup()
                        .addComponent(lblFormatoArquivo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnConsumir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelInputInLayout.createSequentialGroup()
                        .addComponent(xlblLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(xtxtLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(xlblPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pwdPassword))
                    .addGroup(jPanelInputInLayout.createSequentialGroup()
                        .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblListCodSTI)
                            .addComponent(lblDataBaseInfo)
                            .addComponent(lblDtHrEnvio)
                            .addGroup(jPanelInputInLayout.createSequentialGroup()
                                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblDtInitEnvio)
                                    .addComponent(lblDtFinalEnvio))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jxDtFinalEnvio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jxDtInitEnvio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(43, 43, 43)
                                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTmInitEnvio)
                                    .addComponent(lblTmFinalEnvio))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(spinTmInitEnvio, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(spinTmFinalEnvio, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(lblPasta, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInputInLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInputInLayout.createSequentialGroup()
                                .addComponent(txtPasta, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSelecionarPasta, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInputInLayout.createSequentialGroup()
                                .addComponent(lblStatusInfo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(43, 43, 43)
                                .addComponent(lblAtuacaoComprom)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboAtuacaoComprom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelInputInLayout.createSequentialGroup()
                                .addComponent(lblDtInitial)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dtInitial, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(57, 57, 57)
                                .addComponent(lblDtFinal)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dtFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanelInputInLayout.setVerticalGroup(
            jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInputInLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(xlblLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pwdPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(xtxtLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(xlblPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(lblDataBaseInfo)
                .addGap(7, 7, 7)
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDtInitial)
                    .addComponent(dtInitial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDtFinal)
                    .addComponent(dtFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(lblDtHrEnvio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDtInitEnvio)
                    .addComponent(jxDtInitEnvio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTmInitEnvio)
                    .addComponent(spinTmInitEnvio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDtFinalEnvio)
                    .addComponent(jxDtFinalEnvio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTmFinalEnvio)
                    .addComponent(spinTmFinalEnvio, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStatusInfo)
                    .addComponent(comboStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAtuacaoComprom)
                    .addComponent(comboAtuacaoComprom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblListCodSTI)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(paneListCodSTI, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPasta)
                    .addComponent(btnSelecionarPasta)
                    .addComponent(txtPasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelInputInLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFormatoArquivo)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConsumir, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelInputLayout = new javax.swing.GroupLayout(jPanelInput);
        jPanelInput.setLayout(jPanelInputLayout);
        jPanelInputLayout.setHorizontalGroup(
            jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInputLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAmbiente)
                .addGap(18, 18, 18)
                .addComponent(radioHomolog, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(radioProducao)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanelInputIn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelInputLayout.setVerticalGroup(
            jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInputLayout.createSequentialGroup()
                .addGroup(jPanelInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAmbiente)
                    .addComponent(radioHomolog)
                    .addComponent(radioProducao))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelInputIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        textAreaPrompt.setEditable(false);
        textAreaPrompt.setColumns(20);
        textAreaPrompt.setLineWrap(true);
        textAreaPrompt.setRows(5);
        textAreaPrompt.setComponentPopupMenu(textAreaPopupMenu);
        textAreaPrompt.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jScrollPane1.setViewportView(textAreaPrompt);

        javax.swing.GroupLayout jPanelOutputLayout = new javax.swing.GroupLayout(jPanelOutput);
        jPanelOutput.setLayout(jPanelOutputLayout);
        jPanelOutputLayout.setHorizontalGroup(
            jPanelOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanelOutputLayout.setVerticalGroup(
            jPanelOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jPanelFooter.setBackground(jPanelHeader.getBackground());

        lblVersion.setForeground(new java.awt.Color(255, 255, 255));
        lblVersion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblVersion.setText("Versão XXXXXX");

        javax.swing.GroupLayout jPanelFooterLayout = new javax.swing.GroupLayout(jPanelFooter);
        jPanelFooter.setLayout(jPanelFooterLayout);
        jPanelFooterLayout.setHorizontalGroup(
            jPanelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFooterLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblVersion, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanelFooterLayout.setVerticalGroup(
            jPanelFooterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblVersion, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
        );

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelFooter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelFooter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Preenche alguns coampos com valores iniciais definidos nos requisitos do aplicativo
     */
    @SuppressWarnings("unchecked")
    private void initFieldsValues() {
        Config config = Config.getInstance();
        radioHomolog.setSelected(config.isHomologacao());
        radioProducao.setSelected(!config.isHomologacao());

        /**
         * Inicialiando os campos de Data/Hora de Envio Inicial e Final
         */
        DataHoraEnvioInitializer.initialize(jxDtInitEnvio, spinTmInitEnvio, jxDtFinalEnvio, spinTmFinalEnvio);

        txtPasta.setText(config.getProperty(Config.LAST_SAVE_PATH_KEY));

        //comboStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Todas", "Enviada", "Reenviada", "Cancelada Man.", "Cancelada Aut.", "Rejeitada" }));
        ((DefaultComboBoxModel)comboStatus.getModel()).addElement("Reenvio de Cota");

        if (isTeste) {
            Date dt = com.galgo.utils.DateUtils.asDate(LocalDate.of(2015, 10, 9));
            Date tmInitEnvio = com.galgo.utils.DateUtils.asDate(LocalDateTime.of(1900, 1, 1, 18, 0));
            Date tmFinalEnvio = com.galgo.utils.DateUtils.asDate(LocalDateTime.of(1900, 1, 1, 21, 0));

            jxDtInitEnvio.setDate(dt);
            jxDtFinalEnvio.setDate(dt);
            spinTmInitEnvio.setValue(tmInitEnvio);
            spinTmFinalEnvio.setValue(tmFinalEnvio);
        }
    }
    // End of variables declaration                   

    //@formatter:off

    private void menuItemClearTextAreaActionPerformed(
        java.awt.event.ActionEvent evt) {                                                      
        textAreaPrompt.setText("");
    }                                                     
    //@formatter:on

    private void lblPreferencesMouseClicked(MouseEvent evt) {                                            
    }                                           

    private void btnSelecionarPastaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarPastaActionPerformed
        log.debug("Botão btnSelecionarPasta clicado...");
        final File file = FileChooserUtils.chooseFolder();
        if (file == null) {
            return;
        }
        try {
            folderToSave = file;
            txtPasta.setText(file.getCanonicalPath());
        } catch (IOException e) {
            throw new ApplicationException(e);
        }
    }//GEN-LAST:event_btnSelecionarPastaActionPerformed

    private void btnConsumirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsumirActionPerformed
        String clickTimestamp = LocalDateTime.now().toString();
        log.info("****************************************************************************");
        log.info("Click do botão em {}", clickTimestamp);
        log.info("****************************************************************************");

        System.setProperty("plcota.click.timestamp", clickTimestamp);

        // Se data/hora envio final em branco, preenche com a data/hora atual
        if (jxDtFinalEnvio.getDate() == null && StringUtils.isBlank(jxDtFinalEnvio.getEditor().getText())) {
            DataHoraEnvioInitializer.initializeFinal(jxDtFinalEnvio, spinTmFinalEnvio);
        }

        // Adiciona os horários dos jspinners às datas (campos data/hora de envio de informação)
        addTimesToDates();

        final SimpleDateFormat dtFmt = new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss.SSS");
        log.debug("dtTmInitEnvio={}, dtTmFinalEnvio={}", dtTmInitEnvio == null ? "NULL" : dtFmt.format(dtTmInitEnvio),
            dtTmFinalEnvio == null ? "NULL" : dtFmt.format(dtTmFinalEnvio));

        if (!validateteFields()) {
            return;
        }

        // Salvando o conteúdo do formulário no arquivo de configurações do usuário
        saveFields();

        final Date dtInit = dtInitial.getDate();
        final Date dtFin = dtFinal.getDate();
        log.debug("dtIni={}; dtFin={}", dtInit, dtFin);

        //final int qtMax = Integer.parseInt(txtQtMaxElement.getText());
        final int qtMax = 0;

        String cdSTIs = StringUtils.trimToNull(txtCdSTIs.getText());
        StringBuffer errMsg = new StringBuffer();
        final List<Integer> cdSTIList = Lists.newArrayList();
        if (StringUtils.isNotBlank(cdSTIs)) {
            cdSTIList.addAll(FundoUtils.splitCdSTI(cdSTIs, errMsg));
        }

        if (StringUtils.isNotBlank(errMsg.toString())) {
            appendPromptMsg(errMsg.toString());
        }

        Path folder = FileSystems.getDefault().getPath(txtPasta.getText());

        try {
            final String selectedAtuacaoStr = ((String) comboAtuacaoComprom.getSelectedItem()).toUpperCase();
            new SwingWorkerImpl(cdSTIList, dtInit, dtFin, dtTmInitEnvio, dtTmFinalEnvio, qtMax, 1, 0,
                comboStatus.getSelectedIndex(), folder, jComboBox1.getSelectedItem().equals("CSV")).setAtuacao(
                Atuacao.valueOf(selectedAtuacaoStr)).execute();
        } catch (Exception e) {
            log.error(null, e);
            appendPromptMsg(ERRO_STR + e.getMessage() + "\n");
        }
    }//GEN-LAST:event_btnConsumirActionPerformed

    private void radioProducaoActionPerformed(java.awt.event.ActionEvent evt) {                                              
        chooseAmbiente(evt);
    }                                             

    private void radioHomologActionPerformed(java.awt.event.ActionEvent evt) {                                             
        chooseAmbiente(evt);
    }                                            

    /**
     * Salvando valores do formulário no arquivo de configuração do usuário
     */
    private void saveFields() {
        log.info("Salvando o conteúdo do formulário no arquivo de configurações do usuário");
        final Config cfg = Config.getInstance();
        cfg.setUsername(xtxtLogin.getText());
        cfg.setPassword(String.valueOf(pwdPassword.getPassword()));
        cfg.addProperty(Config.LAST_SAVE_PATH_KEY, txtPasta.getText());
        cfg.addProperty(Config.TS_LAST_ENVIO_FINAL, com.galgo.utils.DateUtils.asLocalDateTime(dtTmFinalEnvio));
        cfg.addProperty(Config.LISTA_COD_STI, txtCdSTIs.getText());
    }

    /**
     * A invisilidade do texto é feita alterando a cor da fonte para ficar igual à do fundo
     *
     * @param label
     * @param spinner
     * @param visible Se false, torna a fonte invisível.
     */
    private void setSpinEditorTextVisible(JLabel label, JSpinner spinner, boolean visible) {
        label.setVisible(visible);
        spinner.setVisible(visible);
/*
        if (visible) {
            ((JSpinner.DateEditor) spinner.getEditor()).getTextField().setForeground(spinnerForeground);
        } else {
            ((JSpinner.DateEditor) spinner.getEditor()).getTextField().setForeground(spinnerBackground);
        }
*/
    }

    /**
     * Método auxiliar apenas para incluir o log do aviso exibido ao usuário
     *
     * @param appConfigKey
     */
    private void showValidationErrMsg(String appConfigKey) {
        String msg = appConfig.getProperty(appConfigKey);
        log.info(msg);
        showWarnWindow(msg);
    }

    private void showWarnWindow(String msg) {
        JOptionPane.showMessageDialog(rootPane, msg, "Aviso", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Método que valida se todos os campos estão válidos
     *
     * @return
     */
    private boolean validateteFields() {
        log.info("Validando os campos");
        appConfig = AppConfig.getInstance();

        // Login
        if (StringUtils.isBlank(xtxtLogin.getText())) {
            showValidationErrMsg("err.login.obrigatorio");
            xtxtLogin.requestFocus();
            return false;
        }

        // Senha
        char[] password = pwdPassword.getPassword();
        if (password == null || password.length == 0) {
            showValidationErrMsg("err.password.obrigatorio");
            return false;
        }

        /**
         * ****************************************************************************
         * Data da Informação ou do Envio - uma delas deve ser preenchida
         * ****************************************************************************
         */
        if (dtInitial.getDate() == null && dtFinal.getDate() == null && dtTmInitEnvio == null
            && dtTmFinalEnvio == null) {
            showValidationErrMsg("err.dt_envio.ou.dt_info.obrigatodio");
            return false;
        }

        /**
         * ****************************************************************************
         * Data da Informação
         * ****************************************************************************
         */
        if (dtInitial.getDate() != null && dtFinal.getDate() != null) {
            if (dtInitial.getDate().after(dtFinal.getDate())) {
                showValidationErrMsg("err.data_final.menor.data_inicial");
                return false;
            }

            Date dtFinMax = DateUtils.addDays(dtInitial.getDate(), 120);
            if (dtFinMax.before(dtFinal.getDate())) {
                showValidationErrMsg("err.intervalo_consulta.maior.permitido");
                return false;
            }
        }

        /**
         * ****************************************************************************
         * Data/Hora de Envio da Informação
         * ****************************************************************************
         */
        if (dtTmInitEnvio != null && dtTmFinalEnvio != null) {
            if (dtTmInitEnvio.equals(dtTmFinalEnvio) || dtTmInitEnvio.after(dtTmFinalEnvio)) {
                showValidationErrMsg("err.data_hora.final.menor.data_hora_inicial");
                return false;
            }

            Date dtFinEnvioMax = DateUtils.addDays(dtTmInitEnvio, 120);
            if (dtFinEnvioMax.before(dtTmFinalEnvio)) {
                showValidationErrMsg("err.intervalo_consulta.maior.permitido");
                return false;
            }
        }

        // Pasta onde salvar os arquivos com as informações
        if (StringUtils.isBlank(txtPasta.getText())) {
            showValidationErrMsg("err.folder_to_save.obrigatorio");
            return false;
        }

        return true;
    }

    /**
     * Abre a janela para salvar arquivo usando um prefixo para o título da caixa de diálogo
     */
    private class SwingWorkerImpl extends SwingWorker<String, Void> {

        private final List<Integer> cdSTIList;
        private final Date dtInit;
        private final Date dtFin;
        private final Date dtInitEnvio;
        private final Date dtFinalEnvio;
        private final int qtMax;
        private Integer firstPage = 1;
        private Integer lastPage;
        private Integer statusInfo = 0;
        private Path folderToSaveInto;
        private boolean csv;
        private WSConsumirPLCota wsConsPLCota;
        private Atuacao atuacao;

        /**
         * @param cdSTIList
         * @param dtInit
         * @param dtFin
         * @param dtInitEnvio
         * @param dtFinalEnvio
         * @param qtMax        É utilizado apenas quando qtMax > 0
         * @param firstPage
         * @param lastPage
         * @param statusInfo
         * @param csv
         */
        public SwingWorkerImpl(List<Integer> cdSTIList, Date dtInit, Date dtFin, Date dtInitEnvio, Date dtFinalEnvio,
            int qtMax, Integer firstPage, Integer lastPage, int statusInfo, Path folderToSaveInto, boolean csv) {
            this.cdSTIList = cdSTIList;
            this.dtInit = dtInit;
            this.dtFin = dtFin;
            this.dtInitEnvio = dtInitEnvio;
            this.dtFinalEnvio = dtFinalEnvio;
            this.qtMax = qtMax;
            this.firstPage = firstPage;
            this.lastPage = INTEGER_ZERO.equals(lastPage) ? null : lastPage;
            if (this.lastPage == null) {
                log.info("Todas as páginas serão consumidas");
            }
            this.statusInfo = statusInfo;
            this.folderToSaveInto = folderToSaveInto;
            this.csv = csv;
            ConsumidorPLCota consumidorPLCota = new ConsumidorPLCota(folderToSaveInto, csv);
            consumidorPLCota.setDtHrEnvioFinal(com.galgo.utils.DateUtils.asLocalDateTime(dtFinalEnvio));
            wsConsPLCota = new WSConsumirPLCota();
            wsConsPLCota.setCallback(consumidorPLCota);
            wsConsPLCota.setCodSTIList(cdSTIList);
            wsConsPLCota.setAmbiente(grpAmbiente.isSelected(radioHomolog.getModel()) ? AmbienteEnum.HOMOLOGACAO :
                                     AmbienteEnum.PRODUCAO);
        }

        public SwingWorkerImpl setAtuacao(Atuacao atuacao) {
            this.atuacao = atuacao;
            return this;
        }

        @Override
        protected String doInBackground() throws Exception {
            if (!StringUtils.isBlank(textAreaPrompt.getText())) {
                appendPromptMsg("\n");
            }

            log.debug("Bloqueando a tela para cliques de mouse e teclado");
            appendPromptMsg("Invocando o Web Service...\n");
            PLCotaUI.this.getGlassPane().addMouseListener(doNothingMouseAdapter);
            PLCotaUI.this.getGlassPane().addKeyListener(doNothingKeyAdapter);
            PLCotaUI.this.getGlassPane().setVisible(true);

            log.debug("Cursor Ampulheta");
            PLCotaUI.this.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            // VPN
            VpnHandler vpnHandler = new VpnHandler();
            Config conf = Config.getInstance();
            vpnHandler.setName(conf.getProperty(Config.VPN_NAME));
            vpnHandler.setLogin(conf.getProperty(Config.VPN_USER));

            vpnHandler.setPwd(conf.getVpnPwd());
            vpnHandler.setControlled("true".equalsIgnoreCase(conf.getProperty(Config.VPN_CONTROLLED)));

            try {
                if (cdSTIList.isEmpty()) {
                    log.info("Lista de códigos STI vazia.");
                } else {
                    log.info("Recuperando PL Cotas para {} fundos", cdSTIList.size());
                }
                try {
                    vpnHandler.turnOn();
                    Document plCotaDocument = invocarWS();
                    if (plCotaDocument != null) {
                        //                        saveAsXml(plCotaDocument, null);
                        JOptionPane.showMessageDialog(rootPane, "Operação concluída", "Mensagem",
                            JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        appendPromptMsg("\nNenhuma informação retornada pelo Sistema Galgo.");
                        JOptionPane.showMessageDialog(rootPane, "Operação concluída. Nenuma informação foi retornada",
                            "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (ApplicationException e) {
                    appendPromptMsg("\n" + e.getMessage());
                    JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Mensagem",
                        JOptionPane.ERROR_MESSAGE);
                } catch (Exception e) {
                    log.error(null, e);
                    appendPromptMsg("Erro na execução: " + e.toString());
                    JOptionPane.showMessageDialog(rootPane, "Ocorreu um erro na execução", "Mensagem",
                        JOptionPane.ERROR_MESSAGE);
                }
            } finally {
                vpnHandler.turnOff();
                log.debug("Cursor normal");
                textAreaPrompt.setCaretPosition(textAreaPrompt.getText().length());
                PLCotaUI.this.getGlassPane().setCursor(Cursor.getDefaultCursor());
                PLCotaUI.this.getGlassPane().setVisible(false);
                PLCotaUI.this.getGlassPane().removeMouseListener(doNothingMouseAdapter);
                PLCotaUI.this.getGlassPane().removeKeyListener(doNothingKeyAdapter);
            }
            return null;
        }

        /**
         * Método intermediário que invoca o método invoke() da classe WSConsumirPLCota, tratando as
         * exceções do Galgo de forma mais amigável.
         *
         * @param ws
         * @return
         */
        private MessageRetornoPLCotaComplexType invocar(WSConsumirPLCota ws) {
            try {
                return wsConsPLCota.invoke();
            } catch (ConsumirFaultMsg e) {
                if (!WSConsumirPLCota.PC_0137.equals(e.getFaultInfo().getConsumirFault().
                    getIdException())) {
                    log.error(null, e);
                    appendPromptMsg(
                        ERRO_STR + e.getFaultInfo().getConsumirFault().getIdException() + " - " + e.getFaultInfo()
                            .getConsumirFault().getDsException() + "\n");
                }
            } catch (SOAPFaultException e) {
                log.error(null, e);
                appendPromptMsg(ERRO_STR + SOAPUtils.getFaultDescription(e) + "\n");
            } catch (ApplicationException e) {
                if (WSConsumirPLCota.PC_0237.equals(e.getMessage())) {
                    String errMsg = "Senha inválida";
                    log.info("{}: {}", e.getMessage(), errMsg);
                    throw new ApplicationException(errMsg);
                } else {
                    log.error(e.toString());
                    throw e;
                }
            } catch (Exception e) {
                log.error(null, e);
                appendPromptMsg(ERRO_STR + e.getMessage() + "\n");
                throw new ApplicationException("Erro ao na execução do Web Service");
            }
            return null;
        }

        /**
         * Invocando o Web Service. Consome as informações de PL Cota para todos os fundos
         * permitidos pelo perfil do usuário. O intervalo de tempo a ser consumido é fatiado
         * de acordo com o valor definido em spinTimeSplit. Caso spinTimeSplit &lt; 0, não há
         * fatiamento.
         */
        private Document invocarWS() {
            log.info("Início");

            // Para verificar a performance desse método
            long startTime = System.nanoTime();
            try {
                populateWS(null);

                wsConsPLCota.setListener(PLCotaUI.this);
                wsConsPLCota.setDownloadAllRecords(lastPage == null); // Trazer todos os registros de PL Cota
                MessageRetornoPLCotaComplexType ret;

                // Fatiando o tempo
                TimeInterval tmIntervTotal = null;
                if (dtInitEnvio != null && dtFinalEnvio != null & sliceTime > 0) {
                    log.info("Fatiando o tempo...");
                    tmIntervTotal = new TimeInterval(dtInitEnvio.getTime(), dtFinalEnvio.getTime());
                    tmIntervTotal.setSplitDuration((Integer.valueOf(sliceTime).longValue()));
                } else {
                    log.info("O tempo não será fatiado");
                }

                int counter = 1;
                if (tmIntervTotal != null) {
                    ret = null;
                    // Looping que traz as informações utilizando o fatiamento do tempo
                    while (tmIntervTotal.hasNext()) {
                        TimeInterval interval = tmIntervTotal.next();

                        Date start = new Date(interval.getStartTime());
                        wsConsPLCota.setDtInitEnvio(start);

                        Date end = new Date(interval.getEndTime());
                        appendPromptMsg("\n\n***********************************\n");
                        appendPromptMsg("Fatia de horário " + counter + ":\nde " + start + " a " + end);
                        appendPromptMsg("\n***********************************\n");
                        counter++;
                        wsConsPLCota.setDtFinEnvio(end);

                        log.info("Intervalo fatiado de {} a {}", start, end);
                        ret = PLCotaUtils.join(ret, invocar(wsConsPLCota));
                    }
                } else {
                    ret = invocar(wsConsPLCota);
                }

                //            } finally {
                if (ret != null) {
                    /**
                     * A estrutura salva em arquivo deve ser semelhante à do Download pelo Portal. Isto
                     * é feito nas duas linhas abaixo.
                     */
                    Document plCotaDocument = new Document();
                    plCotaDocument.setPricRptV04(ret.getPricRptV04());

                    return plCotaDocument;
                }
            } catch (ApplicationException e) {
                throw e;
            } catch (Exception e) {
                log.error(null, e);
                throw new ApplicationException("Erro ao invocar o Web Service", e);
            } finally {
                double elapsedTime = (System.nanoTime() - startTime) / Math.pow(10, 9);
                appendPromptMsg("Tempo total de execução: " + elapsedTime + " seg");
            }
            return null;
        }

        private void populateWS(Integer cdSTI) {
            wsConsPLCota.setUser(xtxtLogin.getText(), String.valueOf(pwdPassword.getPassword()));
            wsConsPLCota.setDtInitInfo(dtInit);
            wsConsPLCota.setDtFinInfo(dtFin);
            wsConsPLCota.setDtInitEnvio(dtInitEnvio);
            wsConsPLCota.setDtFinEnvio(dtFinalEnvio);
            if (qtMax > 0) {
                wsConsPLCota.setQtMaxElement(qtMax);
            }
            wsConsPLCota.setFirstPage(firstPage);
            wsConsPLCota.setLastPage(lastPage);
            if (!statusInfo.equals(INTEGER_ZERO)) {
                wsConsPLCota.setStatus(statusInfo);
            }
            wsConsPLCota.setAtuacao(atuacao);
            if (cdSTI != null) {
                wsConsPLCota.setCodSTI(cdSTI);
            }
        }

        /**
         * Salva um objeto no formato XML em um arquivo. Se o objeto for MessageRetornoPLCotaComplexType,
         * altero a estrutura salva para Document, tornando o XML semelhante ao do Download pelo Portal.
         *
         * @param jaxbObject Objeto que será transformado em XML (marshalling) e salvo em arquivo.
         * @param fileToSave Arquivo onde será salvo o XML
         * @param errMsg     Mensagem de erro apresentada caso o arquivo não seja salvo
         */
        private void save(Object jaxbObject, File fileToSave, String errMsg) {
            try {
                Object objectToSave = jaxbObject;
                if (jaxbObject instanceof MessageRetornoPLCotaComplexType) {
                    Document document = new Document();
                    document.setPricRptV04(((MessageRetornoPLCotaComplexType) jaxbObject).getPricRptV04());
                    objectToSave = document;
                }
                Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave), "UTF-8"));
                JAXB.marshal(objectToSave, out);
            } catch (Exception e) {
                log.error(null, e);
                JOptionPane.showMessageDialog(PLCotaUI.this, errMsg, "Erro", JOptionPane.ERROR_MESSAGE);
                throw new ApplicationException();
            }
        }

        /**
         * Cria o nome do arquivo a partir do cdSTI (cdSTI + ".xml")
         *
         * @param ret
         * @param cdSTI
         * @param folderToSave
         */
        private void saveAsXml(MessageRetornoPLCotaComplexType ret, Integer cdSTI, File folderToSave) {
            String fileName = cdSTI + ".xml";
            File fileToSave = new File(folderToSave, fileName);
            save(ret, fileToSave, "Erro ao salvar o arquivo " + fileName);
        }
    }
}
