package br.com.galgo.plcotaswingui.ui

import br.com.galgo.plcotaswingui.utils.AppConfig
import com.galgo.utils.ApplicationException
import javafx.application.Platform
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.input.KeyCode
import javafx.stage.Modality
import javafx.stage.Stage
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.swing.*

/**
 * Created by valdemar.arantes on 25/05/2016.*/
class PreferencesPopupMenu extends JPopupMenu {

    private static final Logger log = LoggerFactory.getLogger(PreferencesPopupMenu.getSuperclass());

    JMenuItem itemVpnPrefs;
    JMenuItem itemConfigPrefs;

    public PreferencesPopupMenu() {
        itemVpnPrefs = new JMenuItem(text: "Configuração da VPN")
        itemVpnPrefs.addActionListener {
            openPreferencesDialog("app.window.prefs_vpn.title", "VpnPreferences.fxml", "err.prefs.vpn")
        };
        add(itemVpnPrefs)

        itemConfigPrefs = new JMenuItem(text: "Alterar pasta do arquivo de configuração")
        itemConfigPrefs.addActionListener {
            openPreferencesDialog("app.window.prefs_config.folder", "ConfigFolderPreferences.fxml", "err.prefs.vpn")
        };
        add(itemConfigPrefs)
    }

    private void openPreferencesDialog(String titleKey, String fxmlFileName, String errMsg) {
        Platform.runLater {
            AppConfig appConf = AppConfig.getInstance();
            try {
                Stage stage = new Stage();
                stage.setTitle(appConf.getProperty(titleKey));
                stage.initModality(Modality.APPLICATION_MODAL);
                //stage.initOwner(primaryStage);
                Parent root = FXMLLoader.load(PreferencesPopupMenu.class.getResource(fxmlFileName));
                Scene scene = new Scene(root);
                scene.setOnKeyPressed({ev ->
                    if (KeyCode.ESCAPE.equals(ev.getCode())) {
                        log.debug("Tecla ESC pressionada. Fechando a janela de preferências...");
                        stage.close();
                    }
                });

                stage.setScene(scene);
                stage.setResizable(false);
                log.debug("Aguardando a Tela de prederências...");
                stage.showAndWait();
                log.debug("Tela de preferências fechada.");
            } catch (ApplicationException e) {
                log.error(e.getMessage(), e);
                showWarnWindow(e.getMessage());
            } catch (Exception e) {
                log.error(null, e);
                showWarnWindow(appConf.getProperty(errMsg));
            }
        };
    }

    private def showWarnWindow(String s) {
        Alert.show(s, javafx.scene.control.Alert.AlertType.WARNING);
    }
}
