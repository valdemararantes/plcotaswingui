/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcotaswingui.ui;


import br.com.galgo.plcotaswingui.utils.Config;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

/**
 * Fábrica de FileChoosers
 *
 * @author valdemar.arantes
 */
public class FileChooserFactory {

    private FileChooserFactory() {
    }

    /**
     * Cria um JFileChooser para selecionar uma pasta onde serão salvos os arquivos.
     *
     * @return
     */
    public static JFileChooser newSaveToDir(String title, String buttonText) {

        // Abre caixa para selecionar arquivo XML
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle(title);
        fileChooser.setApproveButtonText(buttonText);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);

        // Recuperando a pasta do último acesso efetuado
        String lastSavedPath = Config.getInstance().getString(Config.LAST_SAVE_PATH_KEY);
        if (StringUtils.isNotBlank(lastSavedPath)) {
            fileChooser.setCurrentDirectory(new File(lastSavedPath));
        }

        return fileChooser;
    }

    /**
     * Cria um JFileChooser para salvar arquivos de extensão parametrizável
     *
     * @param extension Extensão do arquivo a ser salvo
     * @return
     */
    public static JFileChooser newSaveTypedFile(String extension) {
        // Abre caixa para selecionar arquivo com a extensão <code>extension</code>
        JFileChooser selecionar = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Arquivos (*." + extension + ")", extension);
        selecionar.setDialogTitle("Escolha o nome do arquivo");
        selecionar.setApproveButtonText("Salvar");
        selecionar.setAcceptAllFileFilterUsed(false);
        selecionar.setMultiSelectionEnabled(false);
        selecionar.setFileFilter(filter);

        // Recuperando a pasta do último acesso efetuado
        String lastSavedPath = Config.getInstance().getString(Config.LAST_SAVE_PATH_KEY);
        if (StringUtils.isNotBlank(lastSavedPath)) {
            selecionar.setCurrentDirectory(new File(lastSavedPath));
        }

        return selecionar;
    }

    /**
     * Cria um JFileChooser para salvar arquivos XML
     *
     * @return
     */
    public static JFileChooser newSaveXML() {
        return newSaveTypedFile("xml");
    }

}
