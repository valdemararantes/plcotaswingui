package br.com.galgo.plcotaswingui.ui
import br.com.galgo.plcotaswingui.utils.AppConfig
import br.com.galgo.plcotaswingui.utils.Config
import br.com.galgo.plcotaswingui.utils.CryptoUtils
import com.google.common.collect.Maps
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.GridPane
import javafx.stage.Stage
import org.apache.commons.lang.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
/**
 * Created by valdemar.arantes on 04/05/2016.
 */
public class VpnPreferences {

    private static final Logger log = LoggerFactory.getLogger(VpnPreferences.class);
    private AppConfig appConf = AppConfig.getInstance();
    private Config userConf = Config.getInstance();

    @FXML private GridPane bodyPane;

    @FXML private CheckBox chkEnabled;

    @FXML private Label lblLogin;

    @FXML private TextField txtLogin;

    @FXML private Label lblName;

    @FXML private TextField txtName;

    @FXML private Label lblPwd;

    @FXML private PasswordField pwd;

    @FXML private Button btnConfirmar;

    @FXML private Button btnCancelar;

    public VpnPreferences() {
    }

    @FXML private void handleBtnCancelarAction(ActionEvent event) {
        log.info("Botão cancelar clicado. Fechando a popup window...");
        ((Stage) bodyPane.getScene().getWindow()).close();
    }

    @FXML private void handleBtnConfirmarAction(ActionEvent event) {
        log.info("Click no botão Confirmar");
        saveFormValues();
        ((Stage) bodyPane.getScene().getWindow()).close();
    }

    @FXML void handleBtnConfirmarKeyPressed(KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            log.info("ENTER no botão Confirmar");
            saveFormValues();
            ((Stage) bodyPane.getScene().getWindow()).close();
        }
    }

    @FXML void initialize() {
        initializeFormValues();
    }

    private void initializeFormValues() {
        initializeTextField(txtLogin, "app.vpn.user");
        initializeTextField(txtName, "app.vpn.name");
        initializeTextField(pwd, "app.vpn.pwd");
        if (StringUtils.isNotBlank(userConf.getProperty("app.vpn.controlled"))) {
            chkEnabled.setSelected("true".equalsIgnoreCase(userConf.getProperty("app.vpn.controlled")));
        }
    }

    private void initializeTextField(TextField txtField, String configKey) {
        if (StringUtils.isNotBlank(userConf.getProperty(configKey))) {
            if ("app.vpn.pwd".equals(configKey)) /* É senha */ {
                txtField.setText(userConf.getVpnPwd())

/*
                if (StringUtils.isNotBlank((userConf.getProperty(configKey)))) {
                    try {
                        txtField.setText(CryptoUtils.decrypt(userConf.getProperty(configKey)));
                    } catch (ApplicationException e) {
                        // Tratamento para quando o usuário já usava a senha configurada no arquivo
                        // de configuração de forma aberta.
                        final Throwable rootCause = ExceptionUtils.getRootCause(e);
                        if (rootCause instanceof IllegalBlockSizeException
                            || rootCause instanceof NumberFormatException) {
                            log.info("Erro ao descriptografar a senha da VPN, provavelmente porque"
                                + " o próprio usuário a digitou no arquivo de configuração.");
                        }
                    }
                }
*/
            } else /* Não é senha */ {
                txtField.setText(userConf.getProperty(configKey));
            }
        }
    }

    private void saveFormValues() {
        log.info("Salvando valores do formulário");

        Map<String, String> props = Maps.newHashMap();
        props.put("app.vpn.user", txtLogin.getText());
        props.put("app.vpn.name", txtName.getText());
        props.put("app.vpn.pwd", CryptoUtils.encrypt(pwd.getText()));
        props.put("app.vpn.controlled", String.valueOf(chkEnabled.isSelected()));

        userConf.addProperties(props);
    }
}
