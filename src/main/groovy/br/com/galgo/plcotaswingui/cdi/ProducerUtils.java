package br.com.galgo.plcotaswingui.cdi;

import br.com.galgo.plcota.WSConsumirPLCota;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

/**
 * Created by valdemar.arantes on 04/02/2016.
 */
@ApplicationScoped
public class ProducerUtils {
    @Produces
    public WSConsumirPLCota getWSConsumirPLCota() {
        return new WSConsumirPLCota();
    }
}
