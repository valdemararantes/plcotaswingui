package br.com.galgo.plcotaswingui.utils

import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType
import com.galgo.utils.xml.XMLUtils
import iso.std.iso._20022.tech.xsd.reda_001_001.Document
import iso.std.iso._20022.tech.xsd.reda_001_001.ObjectFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class XMLGenerator {
    /**
     * Created by valdemar.arantes on 24/03/2016.*/
    private static final Logger log = LoggerFactory.getLogger(XMLGenerator.class)
    File xmlFile
    MessageRetornoPLCotaComplexType pojo
    File configFile
    def columnSeparator = ';'
    def lineSeparator = System.getProperty("line.separator")


    XMLGenerator(xmlFile, pojo, configFile = null) {
        log.debug("xmlFile = $xmlFile; pojo = $pojo; configFile = $configFile")

        Objects.requireNonNull(xmlFile, "xmlFile é obrigatório");
        Objects.requireNonNull(pojo, "pojo é obrigatório");

        this.xmlFile = xmlFile
        this.pojo = pojo
        this.configFile = configFile
    }

    void write() {
        Document document = new Document()
        document.pricRptV04 = pojo.pricRptV04
        XMLUtils.marshallToFile(new ObjectFactory().createDocument(document), xmlFile);
    }
}
