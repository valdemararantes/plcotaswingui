package br.com.galgo.plcotaswingui.utils

import br.com.galgo.plcota.utils.ConsumidorCallback
import br.com.galgo.plcotaswingui.ui.PLCotaUI
import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.file.Path
import java.time.LocalDateTime

/**
 * Created by valdemar.arantes on 28/03/2016.
 */
class ConsumidorPLCota implements ConsumidorCallback {

    private static final Logger log = LoggerFactory.getLogger(ConsumidorPLCota.class)
    private boolean csv = false;
    private Path folderToSaveInto;
    LocalDateTime dtHrEnvioFinal;

    ConsumidorPLCota(folderToSaveInto, boolean csv = false) {
        this.csv = csv
        this.folderToSaveInto = folderToSaveInto
    }

    @Override
    void execute(Object message) {
        log.info("CALLBACK");
        if (!message instanceof MessageRetornoPLCotaComplexType) {
            log.error("Esta funcao de Callback espera receber uma mensagem do tipo MessageRetornoPLCotaComplexType " +
                    "mas recebeu do tipo {}.", message.class.name)
        }
        try {
            MessageRetornoPLCotaComplexType response = message
            log.debug('response page = {}', response?.pricRptV04?.msgPgntn?.pgNb)
            File savedFile = new FileHandler(response, folderToSaveInto, csv, dtHrEnvioFinal).save()
            log.info("Arquivo {} salvo", savedFile.canonicalPath)
            PLCotaUI.instance?.notify(null, "Arquivo " + savedFile.canonicalPath + " salvo")
        } catch (e) {
            log.error(null, e);
            PLCotaUI.instance?.notify(null, "Erro ao salvar o arquivo com as informações retornadas pelo Sistema Galgo")
        }
    }
}
