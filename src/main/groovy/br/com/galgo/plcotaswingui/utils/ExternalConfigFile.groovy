package br.com.galgo.plcotaswingui.utils

import org.apache.commons.lang.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by valdemar.arantes on 30/05/2016.*/
public class ExternalConfigFile {

    private static final Logger log = LoggerFactory.getLogger(ExternalConfigFile.class)
    static final String CONFIG_FILE_PATH = "config-file.path"
    static final int ERR_CODE_APP_PROPERTIES_NOT_FOUND = -10
    static com.galgo.utils.Config instance

    /**
     * Carrega o arquivo externo de configurações do aplicativo utilizando as seguintes regras:
     * <ul>
     *
     * <li>Se a variável de ambiente <code>config-file.path</code> estiver definida, utiliza seu valor como caminho
     * do arquivo;</li>
     *
     * <li>Caso contráveio, utiliza o arquivo app.properties encontrado na raiz do classpath</li>
     *
     * </ul>
     * <p>Caso o arquivo não seja encontrado, encerra o aplicativo com código -10.</p>
     */
    static def loadExternalConfigFile() throws URISyntaxException {
        File externalConfigFile;

        // Arquivo definido na variável de ambiente config-file.path
        final String externalConfigFilePathStr = System.getProperty(CONFIG_FILE_PATH);
        log.info("externalConfigFilePathStr={}", externalConfigFilePathStr);
        if (StringUtils.isNotBlank(externalConfigFilePathStr)) {
            log.info("Carregando arquivo externo de configurações do app definido na variável de ambiente: {}", externalConfigFilePathStr);
            externalConfigFile = new File(externalConfigFilePathStr);
        } else {
            final URL appUrl = Thread.currentThread().getContextClassLoader().getResource("app.properties");
            log.info("Carregando arquivo externo de configurações do app a partir do classpath: {}", appUrl.toString());
            externalConfigFile = new File(appUrl.toURI());
        }

        if (!externalConfigFile.isFile()) {
            log.error("O arquivo de configuração {} não foi encontrado. Encerrando o aplicativo...",
                externalConfigFile.getAbsolutePath());
            System.exit(ERR_CODE_APP_PROPERTIES_NOT_FOUND);
        }

        instance = new com.galgo.utils.Config(externalConfigFile);
        return instance;
    }

}
