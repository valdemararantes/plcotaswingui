package br.com.galgo.plcotaswingui.utils;

import br.com.galgo.plcotaswingui.ui.ConfigFolderPreferences;
import com.galgo.utils.ApplicationException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Properties;

/**
 * Carrega as configuracoes do aplicativo
 *
 * @author valdemar.arantes
 */
public class Config {

    public static final String NS_SCH_FUNDO = "http://www.stianbid.com.br/SchemaFundo";
    public static final String NS_SCH_COMMON = "http://www.stianbid.com.br/Common";
    public static final String USERNAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";
    public static final String WS_ADDRESS_HOMOLOG_KEY = "ws.address.homolog";
    public static final String WS_ADDRESS_PRODUCAO_KEY = "ws.address.producao";
    public static final String AMBIENTE_KEY = "ambiente";
    public static final String AMBIENTE_VALUE_PROD = "producao";
    public static final String AMBIENTE_VALUE_HOMOL = "homologacao";
    public static final String CONNECTION_RECEIVE_TIMEOUT = "connection.receive.timeout";
    public static final String LAST_SAVE_PATH_KEY = "xml.last_save.path";
    public static final String TS_LAST_EXECUTION = "timestamp.execution.last";
    private static final Logger log = LoggerFactory.getLogger(Config.class);
    public static final String TS_LAST_ENVIO_FINAL = "date.envio.final.last";
    public static final String LISTA_COD_STI = "lista.cod.sti";
    public static final String VPN_NAME = "app.vpn.name";
    public static final String VPN_USER = "app.vpn.user";
    public static final String VPN_CONTROLLED = "app.vpn.controlled";
    public static final String VPN_PWD = "app.vpn.pwd";
    private static Config instance;
    private Properties props;
    private String configPath;
    private File configFile;
    private boolean configFileCreatedInThisExecution = false;
    private String appName;

    /**
     * @return true se init foi OK
     */
    private Config(String appName, Properties defaultProperties) {
        log.debug("Carregando as configuracoes do aplicativo: appName={}, defaultProperties=", defaultProperties);

        this.appName = appName;

        if (defaultProperties == null) {
            log.warn("Nenhuma propriedade default foi passada");
        }

        // Define o formato do ToString das classes do tipo ValueObject
        ToStringBuilder.setDefaultStyle(ToStringStyle.MULTI_LINE_STYLE);

        configFile = getConfigFile(defaultProperties);

        if (configFile == null) {
            throw new ApplicationException("Não foi possível criar o arquivo de configuração");
        }

        log.info("Abrindo o arquivo {}", configPath);
        InputStream is = null;

        try {
            is = new FileInputStream(configFile);
            props = new Properties();
            props.load(is);
            is.close();
            log.debug("config: " + props.toString());

        } catch (Exception e) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }
            throw new ApplicationException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * Recupera a instância criada no método newInstance. Caso seja nula, lança uma exceção
     * ApplicationException.
     *
     * @return
     */
    public static Config getInstance() {
        if (instance == null) {
            throw new ApplicationException("Classe Config não foi instanciada");
        }
        return instance;
    }

    /**
     * Instancia a classe Config caso ainda não tenha sido instanciada. Se o arquivo de configuração
     * não exisitir, cria um e salva as propriedades passadas como parâmetro.
     *
     * @param appName
     * @param propertiesDefault
     * @return
     */
    public static synchronized Config newInstance(String appName, Properties propertiesDefault) {
        return newInstance(appName, propertiesDefault, false);
    }

    /**
     * Reupera uma instância de Config. Se forceNewInstance = false, funciona exatamente como o método overloaded sem
     * este parâmetro. Se for true, sempre cria uma nova instância.
     *
     * @param appName
     * @param propertiesDefault
     * @param forceNewInstance
     * @return
     */
    public static synchronized Config newInstance(String appName, Properties propertiesDefault,
        boolean forceNewInstance) {
        if (!forceNewInstance) {
            if (instance == null) {
                instance = new Config(appName, propertiesDefault);
            }
        } else {
            instance = new Config(appName, propertiesDefault);
        }
        return instance;
    }

    /**
     * Adiciona e salva as propriedades passadas como parâmetro no arquivo de configuração
     *
     * @param newProperties
     */
    public void addProperties(Map<String, ?> newProperties) {
        props.putAll(newProperties);
        saveProps();
    }

    /**
     * Adiciona e salva a propriedade passada como parâmetro no arquivo de configuração
     *
     * @param key
     * @param value
     */
    public void addProperty(String key, Object value) {
        log.debug("Incluindo propriedade: {}={}", key, value);
        props.setProperty(key, value.toString());
        saveProps();
    }

    public String getCertAlias() {
        return props.getProperty("org.apache.ws.security.crypto.merlin.keystore.alias");
    }

    public String getCertPassword() {
        return props.getProperty("org.apache.ws.security.crypto.merlin.keystore.password");
    }

    public File getConfigFile() {
        return configFile;
    }

    public LocalDate getDate(String key) {
        String dateString = getString(key);
        return LocalDate.parse(dateString, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public LocalDateTime getDateTime(String key) {
        String dateTimeString = getString(key);
        if (StringUtils.isBlank(dateTimeString)) {
            return null;
        }
        return LocalDateTime.parse(dateTimeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    /**
     * Retorna o inteiro configurado para a chave passada com o argumento key. Se chave não estiver
     * definida ou o valor não for um inteiro válido, retorna o argumento defaultValue.
     *
     * @param key Chave
     * @return
     */
    public Integer getInteger(String key, Integer defaultValue) {
        if (!props.containsKey(key)) {
            log.warn("Arquivo de configuração não possui chave={}. Retornado valor default={}", key, defaultValue);
            return defaultValue;
        }

        String val = props.getProperty(key);
        if (StringUtils.isBlank(val)) {
            log.warn("Chave={} configurada com brancos. Retornado valor default={}", key, defaultValue);
            return defaultValue;
        }
        if (!StringUtils.isNumeric(val)) {
            log.warn("Chave={} configurada com valor={}, que não é um número válido. " + "Retornando valor default={}",
                new Object[]{key, val, defaultValue});
            return defaultValue;
        }
        return Integer.valueOf(val);
    }

    /**
     * Recupera a senha do usuário do arquivo de configuração
     *
     * @return
     */
    public String getPassword() {
        if (!props.containsKey(PASSWORD_KEY)) {
            throw new ApplicationException("password indefinido no arquivo de configuracao");
        }
        return CryptoUtils.decrypt(props.getProperty(PASSWORD_KEY));
    }

    /**
     * Sobrepõe as propriedades do arquivo novo nas propriedades desta instância, e já
     * salva o arquivo novo com estas propriedades já combinadas (merge).
     *
     * @param newConfigFile
     * @return
     */
    public Config reload(File newConfigFile) {
        if (!newConfigFile.isFile()) {
            throw new ApplicationException("Arquivo " + newConfigFile.getAbsolutePath() + " não foi encontrado...");
        }

        try {
            log.info("Carregando as propriedades do arquivo {}", newConfigFile.getCanonicalPath());
            final FileReader reader = new FileReader(newConfigFile);
            props.load(reader);
            reader.close();
            configFile = newConfigFile;

            // Necessário para salvar as propriedades do arquivo
            // velho uqe não estavam definidas no novo.
            saveProps();
        } catch (IOException e) {
            throw new ApplicationException("Erro ao ler o arquivo", e);
        }

        return instance;
    }

    /**
     * Salva a senha do usuário no arquivo de configuração
     *
     * @param pwd
     */
    public void setPassword(String pwd) {
        props.setProperty(PASSWORD_KEY, CryptoUtils.encrypt(pwd));
        saveProps();
    }

    /**
     * @return Senha da VPN descriptografada
     */
    public String getVpnPwd() {
        if (StringUtils.isBlank(props.getProperty(VPN_PWD))) {
            return "";
        }

        try {
            return CryptoUtils.decrypt(props.getProperty(VPN_PWD));
        } catch (Exception e) {
            log.warn("Erro ao descriptografar a senha da VPN", e);
            return "";
        }
    }

    /**
     * Se pwd for nulo ou branco, a senha é removida do arquivo de configuração.
     * Caso contrário, a senha será salva criptografada.
     *
     * @param pwd
     */
    public void setVpnPwd(String pwd) {
        if (StringUtils.isBlank(pwd)) {
            log.info("Senha da VPN em branco; removendo do arquivo de configuração");
            props.remove(VPN_PWD);
            saveProps();
            return;
        }

        try {
            props.setProperty(VPN_PWD, CryptoUtils.encrypt(pwd));
        } catch (Exception e) {
            throw new ApplicationException("Erro ao criptografar a senha da VPN", e);
        }
    }

    public Properties getProperties() {
        return props;
    }

    public String getProperty(String propName) {
        return props.getProperty(propName);
    }

    public String getString(String key) {
        return props.getProperty(key);
    }

    public LocalTime getTime(String key) {
        String timeString = getString(key);
        return LocalTime.parse(timeString, DateTimeFormatter.ISO_LOCAL_TIME);
    }

    /**
     * Recupera o login do usuário do arquivo de configuração
     *
     * @return
     */
    public String getUsername() {
        if (!props.containsKey(USERNAME_KEY)) {
            throw new ApplicationException("username indefinido no arquivo de configuracao");
        }
        return props.getProperty(USERNAME_KEY);
    }

    /**
     * Salva o login do usuário no arquivo de configuração
     *
     * @param login
     */
    public void setUsername(String login) {
        props.setProperty(USERNAME_KEY, login);
        saveProps();
    }

    public String getWSAddress() {
        if ("producao".equalsIgnoreCase(props.getProperty(AMBIENTE_KEY))) {
            return props.getProperty(WS_ADDRESS_PRODUCAO_KEY);
        } else {
            return props.getProperty(WS_ADDRESS_HOMOLOG_KEY);
        }
    }

    public boolean isConfigFileCreatedInThisExecution() {
        return configFileCreatedInThisExecution;
    }

    /**
     * Verifica se chave "ambiente" é diferente de "producao"
     *
     * @return
     */
    public boolean isHomologacao() {
        return !"producao".equalsIgnoreCase(props.getProperty(AMBIENTE_KEY));
    }

    /**
     * Caso não seja possível, será utilizada a pasta do aplicativo
     *
     * @return
     */
    private File createInAppFolder() {
        // Caminho do arquivo na pasta do aplicativo
        configPath = SystemUtils.USER_DIR + SystemUtils.FILE_SEPARATOR + "." + appName + SystemUtils.FILE_SEPARATOR
            + "config.properties";
        log.info("configPath={}", configPath);

        File _configFile = new File(configPath);
        if (_configFile.isFile()) {
            log.debug("Arquivo de configuração encontrado.");
            return _configFile;
        }

        log.warn("Arquivo {} não encontrado", _configFile);

        String parent = _configFile.getParent();
        log.debug("_configFile.parent={}", parent);
        File parentFolder = new File(parent);
        if (!parentFolder.exists()) {
            log.info("Pasta {} nao encontrada. Criando...", parentFolder);
            if (!parentFolder.mkdirs()) {
                log.warn("Não foi possível criar a pasta " + parentFolder);
                return null;
            }
        }

        configFileCreatedInThisExecution = true;
        return _configFile;
    }

    /**
     * As configuraçõees tentarão ser mantidas na pasta USER_HOME\.<NOME_APP>\config.properties
     *
     * @return
     */
    private File createInUserHomeFolder() {
        configPath = SystemUtils.USER_HOME + SystemUtils.FILE_SEPARATOR + "." + appName + SystemUtils.FILE_SEPARATOR
            + "config.properties";
        log.info("configPath={}", configPath);

        File _configFile = new File(configPath);
        if (_configFile.isFile()) {
            log.debug("Arquivo {} já existe. Encerrando este método...", _configFile.getAbsolutePath());
            return _configFile;
        }


        String parent = _configFile.getParent();
        log.debug("_configFile.parent={}", parent);
        File parentFolder = new File(parent);
        if (parentFolder.exists() && !parentFolder.isDirectory()) {
            log.warn(parentFolder + " já existe e não é uma pasta!");
            return null;
        } else if (!parentFolder.exists()) {
            log.info("Pasta {} nao encontrada. Criando...", parentFolder);
            if (!parentFolder.mkdirs()) {
                log.warn("Não foi possível criar a pasta " + parentFolder);
                return null;
            }
        }

        configFileCreatedInThisExecution = true;
        return _configFile;
    }

    /**
     * Verifica se o arquivo config.properties existe na pasta
     * <USER_HOME>\.<APP_NAME>. Se existir, retorna o arquivo, se nao existir, retorna as
     * propriedades default passadas no construtor desta classe atribui ao campo
     *
     * @.
     */
    private File getConfigFile(Properties defaultProperties) {
        File _configFile;

        // Tenta carregar as configurações do arquivo configurado em etc/app.properties. Caso não consigo, utiliza
        // o arquivo da psta do usuário
        String configFileProp = ExternalConfigFile.getInstance().getProperty(
            ConfigFolderPreferences.EXT_CONFIG_PATH_KEY);
        if (StringUtils.isNotBlank(configFileProp)) {
            log.info("Carregando o arquivo com as configurações: {}", configFileProp);
            _configFile = new File(configFileProp);
            if (!_configFile.isFile()) {
                log.info("Arquivo {} não carregou. Utilizando a pasta do usuário (default)...");
                _configFile = createInUserHomeFolder();
            }
        } else {
            _configFile = createInUserHomeFolder();
        }

        if (_configFile == null) {
            _configFile = createInAppFolder();
        }
        if (_configFile == null) {
            throw new ApplicationException("Não foi possível criar o arquivo de configuração do aplicativo");
        }

        configFile = _configFile;

        if (configFileCreatedInThisExecution) {
            props = (defaultProperties == null ? new Properties() : defaultProperties);
            log.debug("Arquivo {} criado nessa execução.\nSalvando props default {}", configFile, props);
            saveProps();
        }

        return configFile;
    }

    /**
     * Salva as propriedade no arquivo de configuração
     */
    private void saveProps() {
        OutputStream outStream = null;
        try {
            log.debug("Salvando as configurações em {}", configFile);
            outStream = new FileOutputStream(configFile);
            props.store(outStream, "Arquivo de configuracao");
            outStream.close();
        } catch (Exception e) {
            if (outStream != null) {
                try {
                    outStream.close();
                } catch (IOException ex) {
                }
            }
            throw new ApplicationException(e);
        }
    }
}
