package br.com.galgo.plcotaswingui.utils

import iso.std.iso._20022.tech.xsd.reda_001_001.PriceReportV04

def build() {
    def builder = new ObjectGraphBuilder(
            classLoader: PriceReportV04.class.classLoader,
            classNameResolver: "iso.std.iso._20022.tech.xsd.reda_001_001"
    )

    def price = builder.priceReportV04() {
        pricValtnDtls[
                pricValtnDtl(
                        id: "ID_TESTE"
                )]
    }
    println "price=$price"
}

build()