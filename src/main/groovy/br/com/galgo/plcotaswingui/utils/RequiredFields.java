package br.com.galgo.plcotaswingui.utils;

import javax.swing.*;

/**
 * Manipulação de campos obrigatórios
 *
 * Created by valdemar.arantes on 22/03/2016.
 */
public class RequiredFields {

    private static final String MARKER = " (*)";

    /**
     * Marca os labels dos campos obrigatórios incluindo um (*) no final
     */
    public static void markLabels(JLabel... labels) {
        for (JLabel lbl : labels) {
            String txt = lbl.getText();
            boolean hasColon = (txt.lastIndexOf(":") == (txt.length() - 1));
            if (hasColon) {
                lbl.setText(txt.substring(0, txt.length() - 1) + MARKER + ":");
            } else {
                lbl.setText(txt + MARKER);
            }
        }
    }
}
