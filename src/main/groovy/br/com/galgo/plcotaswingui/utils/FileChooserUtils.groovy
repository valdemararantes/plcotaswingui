package br.com.galgo.plcotaswingui.utils
import br.com.galgo.plcotaswingui.ui.FileChooserFactory
import br.com.galgo.plcotaswingui.ui.PLCotaUI

import javax.swing.*
/**
 * Métodos para apresentação de janelas de seleção de arquivos e pastas
 *
 * Created by valdemar.arantes on 21/03/2016.
 */
class FileChooserUtils {
    static File chooseFolder() {
        JFileChooser fileChooser = FileChooserFactory.newSaveToDir("Selecionar a pasta", "Selecionar")
        if (fileChooser.showOpenDialog(PLCotaUI.instance) == JFileChooser.APPROVE_OPTION) {
            def file = fileChooser.getSelectedFile()
            if (file.exists())
                return file
            else {
                return file.mkdirs()? file : null
            }
        } else {
            return null
        }
    }
}
