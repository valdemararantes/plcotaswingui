package br.com.galgo.plcotaswingui.utils

import com.galgo.utils.XMLGregorianCalendarConversionUtil
import iso.std.iso._20022.tech.xsd.reda_001_001.*
import jodd.bean.BeanUtil
import org.apache.commons.lang.builder.ToStringBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.xml.datatype.XMLGregorianCalendar

/**
 * Created by valdemar.arantes on 24/03/2016.
 */
class CSVGenerator {

    private static final Logger log = LoggerFactory.getLogger(CSVGenerator.class)
    private static NumberFormat BRNumberFormat = NumberFormat.getInstance(new Locale("pt", "BR"))

    File csvFile
    PriceReportV04 pojo
    File configFile
    def columnSeparator = ';'
    def lineSeparator = System.getProperty("line.separator")

    static {
        BRNumberFormat.setMaximumFractionDigits(18)
    }

    CSVGenerator(csvFile, plCotaReturn, configFile = null) {
        log.debug("csvFile = $csvFile; plCotaReturn = $plCotaReturn; configFile = $configFile")

        Objects.requireNonNull(csvFile, "csvFile é obrigatório");
        Objects.requireNonNull(plCotaReturn, "plCotaReturn é obrigatório");

        this.csvFile = csvFile
        //new CharsetToolkit(csvFile).setDefaultCharset(Charset.forName("ISO-8859-1"))
        this.pojo = plCotaReturn.pricRptV04
        this.configFile = configFile
    }

    void write() {
        writeHeader();

        int linha = 0
        try {
            for (priceValuation in pojo.pricValtnDtls) {
                linha++
                def codSTI = priceValuation?.finInstrmDtls?.id?.find { it.othrPrtryId != null }.othrPrtryId.id
                def cnpjFundo = priceValuation.id

                def dtEnvio = priceValuation.valtnDtTm?.dt ?: priceValuation.valtnDtTm.dtTm
                def dt = dtEnvio.toGregorianCalendar().time
                dtEnvio = dt.format("dd-MM-yyyy HH:mm:ss")

                def dtInfo = priceValuation.NAVDtTm?.dt ?: priceValuation.NAVDtTm?.dtTm
                dt = dtInfo.toGregorianCalendar().time
                dtInfo = dt.format("dd-MM-yyyy")

                def pl = (!priceValuation.ttlNAV.empty) ? BRNumberFormat.format(priceValuation.ttlNAV.get(0).value) : null
                def cota = (!priceValuation.NAV?.empty) ? BRNumberFormat.format(priceValuation.NAV.get(0).unit) : null
                def status = priceValuation?.finInstrmDtls?.xtnsn?.find{true}?.plcAndNm
                def vlrStatus = priceValuation?.finInstrmDtls?.xtnsn?.find{true}?.txt

                def linhaToLog = "codSTI=$codSTI; cnpjFundo=$cnpjFundo; dtEnvio=$dtEnvio; dtInfo=$dtInfo; pl = $pl; cota=$cota"
                def columns = [codSTI, cnpjFundo, dtEnvio, dtInfo, pl, cota, status, vlrStatus]
                appendToFile("${columns.join(columnSeparator)}$lineSeparator")
            }
        } catch (e) {
            log.error("Erro ao montar a linha " + linha, e)
        }
    }

    void writeHeader() {
        def headers = ['CodSTI', 'Fundo', 'DtHr Envio', 'Dt Informacao', 'Patrim. Liq.', 'Valor da Cota', 'Status', 'Valor Status']
        csvFile.withWriter('ISO-8859-1') {writer ->
            writer.write("${headers.join(columnSeparator)}$lineSeparator")
        }
    }

    private void appendToFile(String msg) {
        csvFile << msg.getBytes('ISO-8859-1')
    }

    static void main(String... args) {
        Document document = new Document();
        PriceReportV04 priceReportV04 = new PriceReportV04()
        document.pricRptV04 = priceReportV04
        List<PriceValuation3> pricValtnDtls = priceReportV04.pricValtnDtls
        PriceValuation3 priceValuation = new PriceValuation3()
        priceValuation.id="111.111.111-0001-11"
        BeanUtil.setPropertyForced(priceValuation, "NAVDtTm.dt",
                XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(new Date()))
        XMLGregorianCalendar agoraGregorian = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(new Date())
        XMLGregorianCalendar agoraHoraGregorian = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(new Date())
        BeanUtil.setPropertyForced(priceValuation, "valtnDtTm.dtTm", agoraHoraGregorian)
        BeanUtil.setPropertyForced(priceValuation, "NAVDtTm.dt", agoraGregorian)
        ActiveOrHistoricCurrencyAndAmount amount = new ActiveOrHistoricCurrencyAndAmount();
        amount.value = BigDecimal.valueOf(10_0001.25)
        amount.ccy = "BRL"
        priceValuation.ttlNAV << amount
        FinancialInstrumentQuantity1 cota = new FinancialInstrumentQuantity1();
        cota.unit = BigDecimal.valueOf(1.23555)
        priceValuation.NAV << cota
        pricValtnDtls << priceValuation
        FinancialInstrument8 finInstrmDtls = new FinancialInstrument8()
        priceValuation.finInstrmDtls = finInstrmDtls
        SecurityIdentification3Choice choice = new SecurityIdentification3Choice()
        BeanUtil.setPropertyForced(choice, "othrPrtryId.prtryIdSrc", 'SistemaGalgo')
        choice.othrPrtryId.id = '12345-6'
        finInstrmDtls.id << choice;


        log.debug ToStringBuilder.reflectionToString(priceReportV04)


        def csvGen = new CSVGenerator(new File('C:/temp/teste.csv'), document, new File('C:/temp/teste.csv'))
        csvGen.write()
    }
}

import java.text.NumberFormat