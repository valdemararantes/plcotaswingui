package br.com.galgo.plcotaswingui.utils

import com.galgo.utils.ApplicationException
import org.apache.commons.lang.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created by valdemar.arantes on 13/04/2016.*/
class VpnHandler {

    private static final Logger log = LoggerFactory.getLogger(VpnHandler.class)
    private static final AppConfig appConf = AppConfig.getInstance();

    boolean controlled
    String login
    String pwd
    String name

    private prevConnected = false
    private Rasdial rasdial

    /**
     * Se a VPN estava desligada antes de ligá-la no passo anterior, desligo aqui.*/
    def turnOff() {
        try {
            log.debug "prevConnected=${prevConnected}; " +
                "rasdial != null ? ${rasdial != null};" +
                "rasdial.isConnected() ? ${rasdial?.isConnected()}";
            if (!prevConnected && rasdial != null && rasdial.isConnected()) {
                log.info("Desconectando a VPN...")
                log.info("VPN desconectada")
                try {
                    rasdial.disconnect()
                } catch (e) {
                    log.error("Erro ao desconectar a VPN. Status final da VPN: " + rasdial?.isConnected(), e)
                }
            }
        } catch (e) {
            log.warn "Erro ao desconectar a VPN: $e"
            throw new ApplicationException("Erro ao desconectar a VPN", e)
        }
    }

    /**
     * Liga a VPN se o nome, usuário e senha estiverem definidose e se a VPN estava desligada.
     * @return
     */
    def turnOn() {
        if (!controlled) {
            log.info(appConf.getProperty("msg.vpn.not_controlled"));
            return;
        }

        log.info(appConf.getProperty("msg.vpn.controlled"));
        validateFields();

        try {
            log.info("VPN configurada com nome {}", name)
            this.rasdial = new Rasdial(name)
            prevConnected = this.rasdial.connected
            log.info("VPN previamente conectada? {}", prevConnected)
            if (!prevConnected) {
                boolean connected = this.rasdial.connect(login, pwd)
                if (!connected) {
                    String errMsg = "Não foi possível conectar à VPN '$name'"
                    log.warn(errMsg)
                    throw new ApplicationException(errMsg)
                } else {
                    log.info("VPN conectada com sucesso")
                }
            }
        } catch (ApplicationException e) {
            throw e;
        } catch (e) {
            throw new ApplicationException("Erro ao conectar a VPN", e)
        }
    }

    private def validateFields() {
        log.info("Validando os parâmetros da VPN")
        if (StringUtils.isBlank(name)) {
            throw new ApplicationException("O nome da VPN não está preenchido")
        }
        if (StringUtils.isBlank(login)) {
            throw new ApplicationException("O usuário da VPN não está preenchido")
        }
        if (StringUtils.isBlank(pwd)) {
            throw new ApplicationException("A senha da VPN não está preenchida")
        }
    }
}
