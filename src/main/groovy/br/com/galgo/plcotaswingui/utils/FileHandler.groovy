package br.com.galgo.plcotaswingui.utils
import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.file.Path
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Created by valdemar.arantes on 28/03/2016.
 */
public class FileHandler {

    private static final Logger log = LoggerFactory.getLogger(FileHandler.class)
    private static final DateTimeFormatter dtTmFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");

    private Path folder
    private MessageRetornoPLCotaComplexType plCotaRet
    private boolean csv;
    private LocalDateTime dtHrEnvioFinal;

    public FileHandler(MessageRetornoPLCotaComplexType plCotaReturn, Path folderToSaveInto, csv = false, LocalDateTime dtHrEnvioFinal) {
        Objects.requireNonNull(dtHrEnvioFinal)
        folder = folderToSaveInto
        plCotaRet = plCotaReturn
        this.csv = csv
        this.dtHrEnvioFinal = dtHrEnvioFinal
    }

    public File save() {
        File fileToSaveInto = getFileToSaveInto()
        if (csv) {
            new CSVGenerator(fileToSaveInto, plCotaRet).write()
        } else {
            new XMLGenerator(fileToSaveInto, plCotaRet).write()
        }
        log.info("Arquivo {} salvo com sucesso", fileToSaveInto.getCanonicalPath())
        return fileToSaveInto
    }

    /**
     * Gera o nome do arquivo onde os arquivos serão salvos.:<br/>
     * Os nomes dos arquivos salvos na pasta configurada pelo usuário estão no formato YYYYMMDD.HHMM.PAGE
     *
     * @return
     */
    private File getFileToSaveInto() {
        //def yyyymmdd = LocalDateTime.parse(Config.instance.getProperty("date.envio.final.last")).format(dtTmFormatter)
        def yyyymmdd = dtHrEnvioFinal.format(dtTmFormatter)
        def page = plCotaRet.pricRptV04.msgPgntn.pgNb
        def pagePadded = "$page".padLeft(4, "0")
        def name = "${yyyymmdd}_page_${pagePadded}"
        return csv ?  new File(folder.toFile(), "${name}.csv") : new File(folder.toFile(), "${name}.xml")
    }
}
