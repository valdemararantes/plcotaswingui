/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcotaswingui.utils;

import br.com.galgo.plcota.utils.Constants;
import com.galgo.utils.ApplicationException;

import java.io.IOException;
import java.util.Properties;

/**
 * @author valdemar.arantes
 */
public class FirstExecution {

    private boolean firstExecution;

    public static void execute() {
        Properties props = new Properties();
        try {
            props.load(FirstExecution.class.getClassLoader().getResourceAsStream("config_default.properties"));
        } catch (IOException e) {
            throw new ApplicationException(e);
        }
        Config.newInstance(Constants.APP_NAME, props);
    }
}
