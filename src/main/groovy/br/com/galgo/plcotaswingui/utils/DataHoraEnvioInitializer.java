package br.com.galgo.plcotaswingui.utils;

import com.galgo.utils.DateUtils;
import org.jdesktop.swingx.JXDatePicker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by valdemar.arantes on 22/03/2016.
 */
public class DataHoraEnvioInitializer {

    private static final Logger log = LoggerFactory.getLogger(DataHoraEnvioInitializer.class);

    /**
     * Inicializa:
     * <ul>
     * <li>a data/hora de envio inicial com os valores encontrados no arquivo de configuração do usuário com
     * key=date.envio.final.last</li>
     * <li>e a data/hora de envio final com a data/hora atual</li>
     * </ul>
     *
     * @param jxDtInitEnvio
     * @param spinTmInitEnvio
     * @param jxDtFinalEnvio
     * @param spinTmFinalEnvio
     */
    public static void initialize(JXDatePicker jxDtInitEnvio, JSpinner spinTmInitEnvio, JXDatePicker jxDtFinalEnvio,
                                     JSpinner spinTmFinalEnvio) {
        initializeInitial(jxDtInitEnvio, spinTmInitEnvio);
        initializeFinal(jxDtFinalEnvio, spinTmFinalEnvio);
    }

    /**
     * Preenche a data/hora de envio final com a data/hora atual
     * @param jxDtFinalEnvio
     * @param spinTmFinalEnvio
     */
    public static void initializeFinal(JXDatePicker jxDtFinalEnvio, JSpinner spinTmFinalEnvio) {
        Date now = new Date();
        jxDtFinalEnvio.setDate(now);
        spinTmFinalEnvio.setValue(now);
    }

    private static void initializeInitial(JXDatePicker jxDtInitEnvio, JSpinner spinTmInitEnvio) {
        // Recuperando o TS da última execução
        LocalDateTime tsLast = Config.getInstance().getDateTime(Config.TS_LAST_ENVIO_FINAL);

        // Se não encontra, seta o dia para hoje e a hora para 0h
        if (tsLast == null) {
            log.info("Último Timestamp de execução não encontrado. Utilizando data de hoje, 0h");
            tsLast = LocalDate.now().atStartOfDay();
        }

        // Seta o campo DataHora inicial
        Date dt = DateUtils.asDate(tsLast);
        jxDtInitEnvio.setDate(dt);
        spinTmInitEnvio.setValue(dt);
    }

}
