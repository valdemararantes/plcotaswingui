package br.com.galgo.plcotaswingui.cli
import com.beust.jcommander.IStringConverter

import java.time.LocalDateTime
/**
 * Created by valdemar.arantes on 11/04/2016.
 */
class DateTimeConverter implements  IStringConverter<LocalDateTime>{

    /**
     * @return an object of type <T> created from the parameter value.
     */
    @Override
    LocalDateTime convert(String value) {
        return LocalDateTime.parse(value)
    }
}
