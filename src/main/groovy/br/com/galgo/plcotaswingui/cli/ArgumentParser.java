package br.com.galgo.plcotaswingui.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.apache.commons.lang.StringUtils;

/**
 * Created by valdemar.arantes on 11/03/2016.
 */
public class ArgumentParser {

//    private static final Logger log = LoggerFactory.getLogger(ArgumentParser.class);
//    private static OptionParser parser;

    public static Argument parse(String... args) {
        Argument argument = new Argument();
        JCommander jComm = null;
        try {
            jComm = new JCommander(argument, args);
            if (argument.help) {
                jComm.usage();
            } else {
                validate(argument);
            }
        } catch (ParameterException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        } catch (Exception e) {
            e.printStackTrace();
            new JCommander().usage();
            System.exit(-1);
        }
        return argument;
    }

    private static void validate(Argument arg) {
        if (!"P".equalsIgnoreCase(arg.ambiente) && !"H".equalsIgnoreCase(arg.ambiente)) {
            throw new ParameterException("O argumento -amb deve receber os valores P (producao) ou H (homologacao)");
        }

        if (!"P".equalsIgnoreCase(arg.atuacao) &&
            !"C".equalsIgnoreCase(arg.atuacao)) {
            throw new ParameterException("O argumento -atua deve receber os valores P (provedor) ou C (consumidor");
        }

        if (!"XML".equalsIgnoreCase(arg.fileType) &&
            !"CSV".equalsIgnoreCase(arg.fileType)) {
            throw new ParameterException("O argumento -fileType deve receber os valores XML ou CSV");
        }

        if (!arg.folder.isDirectory()) {
            throw new ParameterException("O argumento -folder não foi definido com uma pasta válida");
        }

        // Se o nome da VPN voi passado, o login e a senha da VPN passam a ser obrigatórios
        if (StringUtils.isNotBlank(arg.vpnNome)) {
            if (StringUtils.isBlank(arg.vpnLogin)) {
                throw new ParameterException("O argumento -vpnLogin é obrigatório quando -vpnNome é definido");
            }
            if (StringUtils.isBlank(arg.vpnSenha)) {
                throw new ParameterException("O argumento -vpnSenha é obrigatório quando -vpnNome é definido");
            }
        }
    }
}
