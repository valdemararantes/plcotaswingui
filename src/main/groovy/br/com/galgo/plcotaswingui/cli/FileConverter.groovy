package br.com.galgo.plcotaswingui.cli
import com.beust.jcommander.IStringConverter
/**
 * Created by valdemar.arantes on 11/04/2016.
 */
class FileConverter implements  IStringConverter<File>{
    @Override
    File convert(String value) {
        return new File(value)
    }
}
