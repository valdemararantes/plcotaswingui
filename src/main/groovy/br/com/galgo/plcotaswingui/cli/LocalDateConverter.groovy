package br.com.galgo.plcotaswingui.cli
import com.beust.jcommander.IStringConverter

import java.time.LocalDate
/**
 * Created by valdemar.arantes on 11/04/2016.
 */
class LocalDateConverter implements  IStringConverter<LocalDate>{

    /**
     * @return an object of type <T> created from the parameter value.
     */
    @Override
    LocalDate convert(String value) {
        return LocalDate.parse(value)
    }
}
