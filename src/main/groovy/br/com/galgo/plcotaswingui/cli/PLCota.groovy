package br.com.galgo.plcotaswingui.cli
import br.com.galgo.plcota.WSConsumirPLCota
import br.com.galgo.plcota.utils.Atuacao
import br.com.galgo.plcotaswingui.ui.PLCotaUI
import br.com.galgo.plcotaswingui.utils.AppConfig
import br.com.galgo.plcotaswingui.utils.Config
import br.com.galgo.plcotaswingui.utils.ConsumidorPLCota
import br.com.galgo.plcotaswingui.utils.ExternalConfigFile
import br.com.galgo.plcotaswingui.utils.VpnHandler
import com.galgo.cxfutils.ws.AmbienteEnum
import com.galgo.utils.ApplicationException
import com.galgo.utils.DateUtils
import com.galgo.utils.cert_digital.TrustAllCerts
import com.google.common.collect.Lists
import org.apache.commons.lang.builder.ToStringBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.time.LocalDateTime

/**
 * Created by valdemar.arantes on 11/03/2016.
 */
public class PLCota {

    private static final Logger log = LoggerFactory.getLogger(PLCota.class)
    private boolean vpnPreviouslyConnected = false

    public static void main(String... args) {
        ExternalConfigFile.loadExternalConfigFile();
        log.info("Iniciando o aplicativo. {}", AppConfig.getInstance().getString("app.version", "Sem versão"))
        int returnCode = 0;
        try {
            returnCode = new PLCota().execute(args)
        } catch (Exception e) {
            log.error(null, e)
            returnCode = -3
        } catch (Throwable e) {
            returnCode = -4
        } finally {
            log.info("Fim do aplicativo aplicativo. {}", AppConfig.getInstance().getString("app.version", "Sem versão"))
            System.exit(returnCode)
        }
    }

    public int execute(String... args) {
        final Argument argument = parseParameters(args)
        if (argument.help) {
            return 0;
        }

        initializeConfig();

        // RASDIAL - Conectando na VPN RTM se estiver configurada no
        // arquivo de configuração do usuário
        VpnHandler vpnHandler = new VpnHandler(
                login: argument.vpnLogin,
                pwd: argument.vpnSenha,
                name: argument.vpnNome)
        vpnHandler.turnOn()
/*
        Config conf = Config.instance
        String vpnName = conf.getProperty(Config.VPN_NAME)
        Rasdial rasdial = null
        if (vpnName) {
            log.info("VPN configurada com nome {}", vpnName)
            rasdial = new Rasdial(vpnName)
            vpnPreviouslyConnected = rasdial.connected
            log.info("VPN previamente conectada? {}", vpnPreviouslyConnected)
            if (!vpnPreviouslyConnected) {
                boolean connected = rasdial.connect(conf.getProperty(Config.VPN_USER), conf.getProperty(Config.VPN_PWD))
                log.info("VPN conectada com sucesso? {}", connected)
            }
        } else {
            log.info("Nenhuma VPN foi configurada")
        }
*/

        TrustAllCerts.doIt()
        WSConsumirPLCota wSCons = from(argument);

        ConsumidorPLCota callbackClass = new ConsumidorPLCota(
                argument.folder.toPath(), "CSV".equalsIgnoreCase(argument.fileType));
        if (argument.dtTmEnvioFinal) {
            callbackClass.dtHrEnvioFinal = argument.dtTmEnvioFinal
        } else {
            def now = LocalDateTime.now()
            log.info("Utilizando como data/hora de envio final {}", now)
            callbackClass.dtHrEnvioFinal = now;
        }
        wSCons.callback = callbackClass

        try {
            wSCons.invoke()
            return 0;
        } catch (ApplicationException e) {
            if ("PC.0237".equals(e.getMessage())) {
                log.info("Usuário/Senha inválidos");
                return -1;
            } else {
                log.error(null, e);
                return -2;
            }
        } finally {
            vpnHandler.turnOff()
/*
            if (!vpnPreviouslyConnected && rasdial?.connected ) {
                rasdial.disconnect()
                log.info("VPN desconectada")
            }
*/
        }
    }

    private void initializeConfig() {
        final Properties defaultProps = new Properties();
        defaultProps.load(PLCotaUI.class.getResourceAsStream("/config_default.properties"));
        Config.newInstance("plcota", defaultProps);
    }

    private Argument parseParameters(String... args) {
        final Argument argument = ArgumentParser.parse args
        log.info("argument={}", ToStringBuilder.reflectionToString(argument))
        return argument
    }

    private WSConsumirPLCota from(Argument arg) {
        log.debug("Populando WSConsumirPLCota a partir de Argument")
        WSConsumirPLCota wsCons = new WSConsumirPLCota()

        wsCons.ambiente = arg.ambiente.trim().equalsIgnoreCase("P") ? AmbienteEnum.PRODUCAO : AmbienteEnum.HOMOLOGACAO
        wsCons.setUser arg.login, arg.senha

        // Data base
        wsCons.dtInitInfo = arg.dtInfoInitial ? DateUtils.asDate(arg.dtInfoInitial) : null
        wsCons.dtFinInfo = arg.dtInfoFinal ? DateUtils.asDate(arg.dtInfoFinal) : null

        // Data/Hora de envio
        wsCons.dtInitEnvio = arg.dtTmEnvioInitial ? DateUtils.asDate(arg.dtTmEnvioInitial) : null
        wsCons.dtFinEnvio = arg.dtTmEnvioFinal ? DateUtils.asDate(arg.dtTmEnvioFinal) : null

        wsCons.atuacao = Atuacao.fromChar(arg.atuacao.toUpperCase())
        wsCons.status = Integer.valueOf(arg.statusInfo) > 0 ? Integer.valueOf(arg.statusInfo) : null
        if (arg.codSTIStringList) {
            wsCons.codSTIList = Lists.newArrayList()
            arg.codSTIStringList.each { codSTIString ->
                wsCons.codSTIList.add(Integer.valueOf(codSTIString.replace('-', '')))
            }
        }

        log.debug("ret={}", wsCons);

        return wsCons
    }

}
