package br.com.galgo.plcotaswingui.cli;

import com.beust.jcommander.Parameter;
import com.google.common.collect.Lists;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by valdemar.arantes on 11/03/2016.
 */
public class Argument {
    //@formatter:off
    @Parameter(names = "--help", help = true, description = "Apresenta os parâmetros que podem ser utilizados")
    public boolean help;

    @Parameter(names = {"-login"}, required = true, description = "Login de acesso (Usuario Sistema Externo)")
    public String login;

    @Parameter(names = {"-senha"}, required = true, description = "Senha do usuario")
    public String senha;

    @Parameter(names = {"-amb"}, required = false, description = "Ambiente de Producao (P) ou de Homologacao (H)")
    public String ambiente = "P";

    @Parameter(names = {"-dtBaseInicial"}, required = false, description = "Data base inicial no formato dd/mm/yyyy",
               converter = LocalDateConverter.class)
    public LocalDate dtInfoInitial;

    @Parameter(names = {"-dtBaseFinal"}, required = false, description = "Data base final no formato yyyy-mm-dd",
               converter = LocalDateConverter.class)
    public LocalDate dtInfoFinal;

    @Parameter(names = {"-dtTmEnvioInicial"}, required = false,
               description = "Data/Hora de envio inicial no formato yyyy-MM-ddThh:mm:ss ",
               converter = DateTimeConverter.class)
    public LocalDateTime dtTmEnvioInitial;

    @Parameter(names = {"-dtTmEnvioFinal"}, required = false,
               description = "Data/Hora de envio final no formato yyyy-MM-ddThh:mm:ss ",
               converter = DateTimeConverter.class)
    public LocalDateTime dtTmEnvioFinal;

    public static final String statusInfoOpcoes = "0: Todas; 1: Enviada; 2: Reenviada; 3: Cancelada Man; "
        + "4: Cancelada Aut; 5: Rejeitada";
    @Parameter(names = {"-statusInfo"}, required = false, description = "Status da informação: " + statusInfoOpcoes)
    public String statusInfo = "0";

    @Parameter(names = {"-atua"}, description = "Atuacao no compromisso: P (provedor) ou C (consumidor)")
    public String atuacao = "C";

    @Parameter(names = {"-codSTI"}, required = false, description = "Codigo STI do fundo (Zero ou mais códigos)")
    public List<String> codSTIStringList = Lists.newArrayList();

    @Parameter(names = {"-qtMax"}, required = false, description = "Qtd. maxima de informações por pagina (0 a 500)")
    public int qtMax = 0;

    @Parameter(names = {"-folder"}, required = true,
               description = "Pasta onde os arquivos com as informações serão salvos",
               converter = FileConverter.class)
    public File folder;

    @Parameter(names = {"-fileType"}, required = false,
               description = "Tipo de arquivo a ser salvo: XML ou CSV")
    public String fileType = "XML";

    @Parameter(names = {"-vpnNome"}, required = false,
               description = "Nome da VPN previamente configurada")
    public String vpnNome;

    @Parameter(names = {"-vpnLogin"}, required = false,
               description = "Login da VPN. Deve ser definido no caso de -vpnNome estar definido.")
    public String vpnLogin;

    @Parameter(names = {"-vpnSenha"}, required = false,
               description = "Senha da VPN. Deve ser definida no caso de -vpnNome estar definido.")
    public String vpnSenha;

    //@formatter:on
}
