package br.com.galgo.plcotaswingui.ui;

import javax.swing.*;

/**
 * Created by valdemar.arantes on 06/04/2016.
 */
public class GalgoSpinnerDateEditor extends JSpinner.DateEditor {

    public GalgoSpinnerDateEditor(JSpinner spinner) {
        super(spinner);
    }

    public GalgoSpinnerDateEditor(JSpinner spinner, String dateFormatPattern) {
        super(spinner, dateFormatPattern);
    }


}
