package br.com.galgo.plcotaswingui.ui;

import com.galgo.utils.DateUtils;

import javax.swing.*;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by valdemar.arantes on 04/04/2016.
 */
public class GalgoSpinnerDateModel  extends SpinnerDateModel {

    private boolean valid = false;

    public GalgoSpinnerDateModel() {
        this.valid = false;
    }

    public GalgoSpinnerDateModel(Date value, Comparable start, Comparable end, int calendarField) {
        super(value, start, end, calendarField);
        this.valid = false;
    }

    @Override
    public Object getNextValue() {
        if (valid) {
            return super.getNextValue();
        } else {
            valid = true;
            return attMidnight();
        }
    }

    @Override
    public Object getPreviousValue() {
        if (valid) {
            return super.getPreviousValue();
        } else {
            valid = true;
            return attMidnight();
        }
    }

    @Override
    public Date getDate() {
        return super.getDate();
    }

    @Override
    public Object getValue() {
        return valid ? super.getValue() : null;
    }

    @Override
    public void setValue(Object value) {
        if (value != null) {
            valid = true;
            super.setValue(value);
        } else {
            valid = false;
            super.setValue(attMidnight());
        }
    }

    private Date attMidnight() {
        return DateUtils.asDate(LocalDate.now().atStartOfDay());
    }
}

