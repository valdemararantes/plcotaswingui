import br.com.galgo.plcotaswingui.utils.Config
import org.apache.commons.configuration2.Configuration
import org.apache.commons.configuration2.FileBasedConfiguration
import org.apache.commons.configuration2.PropertiesConfiguration
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder
import org.apache.commons.configuration2.builder.fluent.Parameters

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
/**
 * Created by valdemar.arantes on 22/04/2016.
 */

Parameters params = new Parameters()
builder =
        new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                .configure(params.properties()
                .setFileName('C:\\Users\\valdemar.arantes\\.plcota\\config.properties'));
Configuration config = builder.getConfiguration();
println "app.vpn.name=${config.getProperty('app.vpn.name')}"

config.setProperty('app.vpn.name', 'VPN_FAKE_1')
println "app.vpn.name=${config.getProperty('app.vpn.name')}"

dateTimeString = config.getString(Config.TS_LAST_ENVIO_FINAL)
LocalDateTime tsLast = LocalDateTime.parse(dateTimeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
println "tsLast=$tsLast"