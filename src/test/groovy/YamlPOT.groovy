import org.yaml.snakeyaml.Yaml

import java.time.LocalDateTime

/**
 * Created by valdemar.arantes on 22/04/2016.
 */

def input = '''
version: 1.0
released: 2012-11-30

# Connection parameters
connection:
    url: jdbc:mysql://localhost:3306/db
    poolSize: 5

# Protocols
protocols:
   - http
   - https

# Users
users:
    tom: passwd
    bob: passwd
'''
Yaml yaml = new Yaml()
Object o = yaml.load input
println o

class Staff {
    def firstname, lastname, position
    Date dt
}
input = '''
firstname: John
lastname: Connor
position: Resistance Leader
dt: 1967-03-28T16:30:00
'''
Staff s = new Yaml().load(input)
println s.dump()

class Staff1 {
    def name
    def LocalDateTime ldt
}

def s1 = new Staff1(name: "Neto", ldt: LocalDateTime.now())
println new Yaml().dump(s1)
