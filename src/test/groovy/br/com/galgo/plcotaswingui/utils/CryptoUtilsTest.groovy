package br.com.galgo.plcotaswingui.utils;

import org.junit.Test;

/**
 * Created by valdemar.arantes on 04/05/2016.
 */
public class CryptoUtilsTest {

    @Test
    public void testEncrypt_Decrypt() throws Exception {
        def pwd = 'Treina01';
        def cryptPwd = CryptoUtils.encrypt(pwd)
        println "cryptPwd=$cryptPwd"
        def decrPwd = CryptoUtils.decrypt(cryptPwd)
        println "descrPwd=$decrPwd"
        assert decrPwd.equals(pwd)
    }
}