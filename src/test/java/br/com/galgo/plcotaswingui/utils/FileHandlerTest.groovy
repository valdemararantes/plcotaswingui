package br.com.galgo.plcotaswingui.utils
import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType
import com.galgo.utils.XMLGregorianCalendarConversionUtil
import iso.std.iso._20022.tech.xsd.reda_001_001.*
import jodd.bean.BeanUtil
import org.junit.Test

import javax.xml.datatype.XMLGregorianCalendar
import java.nio.file.Paths
/**
 * Created by valdemar.arantes on 28/03/2016.
 */
public class FileHandlerTest {

    @Test
    public void testSave() throws Exception {
        PriceReportV04 priceReportV04 = new PriceReportV04()

        MessageRetornoPLCotaComplexType retorno = new MessageRetornoPLCotaComplexType()
        retorno.pricRptV04 = priceReportV04

        List<PriceValuation3> pricValtnDtls = priceReportV04.pricValtnDtls
        PriceValuation3 priceValuation = new PriceValuation3()
        priceValuation.id="111.111.111-0001-11"
        BeanUtil.setPropertyForced(priceValuation, "NAVDtTm.dt",
                                      XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(new Date()))
        XMLGregorianCalendar agoraGregorian = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(new Date())
        XMLGregorianCalendar agoraHoraGregorian = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(new Date())
        BeanUtil.setPropertyForced(priceValuation, "valtnDtTm.dtTm", agoraHoraGregorian)
        BeanUtil.setPropertyForced(priceValuation, "NAVDtTm.dt", agoraGregorian)
        ActiveOrHistoricCurrencyAndAmount amount = new ActiveOrHistoricCurrencyAndAmount();
        amount.value = BigDecimal.valueOf(10_0001, 00)
        amount.ccy = "BRL"
        priceValuation.ttlNAV << amount

        FinancialInstrumentQuantity1 cota = new FinancialInstrumentQuantity1();
        cota.unit = BigDecimal.valueOf(1.23)
        priceValuation.NAV << cota
        pricValtnDtls << priceValuation

        FinancialInstrument8 finInstrmDtls = new FinancialInstrument8()
        priceValuation.finInstrmDtls = finInstrmDtls
        SecurityIdentification3Choice choice = new SecurityIdentification3Choice()
        BeanUtil.setPropertyForced(choice, "othrPrtryId.prtryIdSrc", 'SistemaGalgo')
        choice.othrPrtryId.id = '12345-6'
        finInstrmDtls.id << choice;

        new FileHandler(retorno, Paths.get("C:/temp/teste")).setCsv(false).save()
        new FileHandler(retorno, Paths.get("C:/temp/teste")).setCsv(true).save()
    }
}