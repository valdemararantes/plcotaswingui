package br.com.galgo.plcotaswingui.utils;

import br.com.galgo.plcotaswingui.cli.Argument;
import br.com.galgo.plcotaswingui.cli.ArgumentParser;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by valdemar.arantes on 14/03/2016.
 */
public class ArgumentParserTest {

    private static final Logger log = LoggerFactory.getLogger(ArgumentParserTest.class);

    @Test
    public void testParse() throws Exception {
        final Argument arguments = ArgumentParser.parse("-l", "um_login", "-p", "uma_senha");
        log.debug("arguments={}", ToStringBuilder.reflectionToString(arguments));
    }

    @Test
    public void testParseOpcaoObrigatoriaAusente() throws Exception {
        final Argument arguments = ArgumentParser.parse("-p", "uma_senha");
        log.debug("arguments={}", ToStringBuilder.reflectionToString(arguments));
    }
}