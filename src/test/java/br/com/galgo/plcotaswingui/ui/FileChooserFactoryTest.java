package br.com.galgo.plcotaswingui.ui;

import br.com.galgo.plcota.utils.Constants;
import br.com.galgo.plcotaswingui.utils.Config;
import br.com.galgo.plcotaswingui.utils.FileChooserUtils;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Properties;

/**
 * Created by valdemar.arantes on 22/03/2016.
 */
public class FileChooserFactoryTest {

    private static final Logger log = LoggerFactory.getLogger(FileChooserFactoryTest.class);

    @BeforeClass
    public static void initializeClass() {
        Config.newInstance(Constants.APP_NAME, new Properties());
    }

    @Ignore
    public void testNewSaveXML() throws Exception {
    }

    @Ignore
    public void testNewSaveTypedFile() throws Exception {

    }

    @Test
    public void testNewSaveToDir() throws Exception {
        System.setProperty("junit.test.folder", "C:/PastaTemporaria");
        final File file = FileChooserUtils.chooseFolder();
        log.debug("file={}", file != null ? file.getCanonicalPath() : "NULL");
        Assertions.assertThat(file.exists()).isTrue();
        file.delete();
    }
}